package com.xcase.sdk.encoder;

import java.util.HashMap;
import java.util.Map;

import com.xcase.sdk.util.RGBUtil;


public class LZWEncoder {

	public static int ConvertErrorColorCount = 1;
	public static int ConvertErrorNone = 2;
	
	private int outputSize;
	private int[] newColorTable = new int[256]; 
	private int usedCCount = 0;
	
	public int encodeLZW(int[][] rawData, int width, int height, int bytesPerPixel, byte[] encoded) {
		int usedCCount = 0;
		Map<Integer, Integer> colorMap = new HashMap<Integer, Integer>(256);	//key——颜色值		value——计数索引
		byte[] indexBuf = new byte[width * height * 2];
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j <width; ++j) {
				int rgb = rawData[i][j];
				int newColor = RGBUtil.RGB888ToRGB565(rgb);
				int nm = 0;
				if (!colorMap.containsKey(newColor)) {
					//颜色Map中不存在
					colorMap.put(newColor, usedCCount);
					nm = usedCCount;
					this.newColorTable[usedCCount++] = ((newColor & 0xff) << 8) | ((newColor & 0xff00) >> 8);
					if (usedCCount > 128) {
						return ConvertErrorColorCount;
					}
				}else{
					nm = colorMap.get(newColor);
				}
				indexBuf[i*width + j] = (byte)nm;
			}
		}
		encodeBuf(indexBuf, (int)(width * height), encoded);
		this.outputSize = this.outsz;
		this.usedCCount = usedCCount;

		return ConvertErrorNone;
	}

	public int getOutputSize() {
		return outputSize;
	}

	public int[] getNewColorTable() {
		return newColorTable;
	}

	public int getUsedCCount() {
		return usedCCount;
	}
	
	private final int BITS = 8;
	private int outsz = 0;
    private int outbyte = 0;
    private int outbits = 0;
	private int encodeBuf(byte[] buf, int size_in, byte[] outBuf){
		
	    int max = 0;
	    int[] table = new int[1 << BITS];
	    int tableCount = 0;
	    
	    byte[] sign = new byte[256];
	    for (int i = 0; i < size_in; ++i) {
	        if (sign[buf[i]] == 0) {
	            max ++ ;
	            sign[buf[i]] = 1;
	        }
	    }
	    max--;
	    int bits = 0;
	    while (max > 0) {
	        bits ++ ;
	        max >>= 1;
	    }
	    if (bits < 2) {
	        bits = 2;
	    }
	    max = 1 << bits;
	    int CLEAR_CODE = max;
	    int EOF_CODE = max + 1;
	    int crntBits = bits + 1;
	    
	    int crntCode = buf[0];
	    for (int i = 1; i < size_in; ++i) {
	        int v = buf[i];
	        int k = (crntCode << 8) + v;
	        int code = -1;
	        for (int j = 0; j < tableCount; ++j) {
	            if (table[j] == k) {
	                code = j;
	                break;
	            }
	        }
	        if (code >= 0) {
	            crntCode = code + EOF_CODE + 1;
	            continue;
	        }
	        outputCode(crntCode, crntBits, outBuf);
	        if (tableCount + EOF_CODE >= (1 << crntBits) - 1 && crntBits < BITS) {
	            crntBits ++ ;
	        }
	        if (tableCount + EOF_CODE >= (1 << BITS) - 1) {
	            outputCode(CLEAR_CODE, crntBits, outBuf);
	            crntBits = bits + 1;
	            tableCount = 0;
	        } else {
	            table[tableCount ++ ] = k;
	        }
	        crntCode = v;
	    }
	    outputCode(crntCode, crntBits, outBuf);
	    outputCode(EOF_CODE, crntBits, outBuf);
	    if (this.outbits > 0) {
	    	outBuf[this.outsz++] = (byte)(this.outbyte&0xff);
	    }
	    return 0;
	}

	private void outputCode(int crntCode, int crntBits, byte[] outBuf) {
	    this.outbyte |= (crntCode << this.outbits);
	    this.outbits += crntBits;
	    while (this.outbits >= 8) {
	    	outBuf[this.outsz++] = (byte)(this.outbyte&0xff);
	        this.outbyte >>= 8;
	        this.outbits -= 8;
	    }
	}
	
}
