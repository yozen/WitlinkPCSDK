package com.xcase.sdk.util;

public class CRC16Util {
	
	private static int polynomial = 0x1021;
	
	public static int check(byte[] bytes, int initCrc){
		
		int crc = initCrc;
		for (int index = 0; index < bytes.length; index++) {
		    byte b = bytes[index];
		    for (int i = 0; i < 8; i++) {
		        boolean bit = (( (b >> (7-i)) & 1) == 1);
		        boolean c15 = (( (crc >> 15)  & 1) == 1);
		        crc <<= 1;
		        crc &= 0xffff;
		        if (bit)
		        	crc += 1;
		        if (c15)
		        	crc ^= polynomial;
		     }
		}
		crc &= 0xffff;
		for (int i = 0; i < 16; i++)
		{
			boolean c15 = (( (crc >> 15) & 1) == 1);
			crc <<= 1;
			crc &= 0xffff;
			if (c15) 
				crc ^= polynomial;
		}
		crc &= 0xffff;
		
		return crc;
		
	}
}
