package com.xcase.sdk.util;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;

public class RGBUtil {
	//RGB888תRGB565
	public static int RGB888ToRGB565(int color){
		BufferedImage img565 = new BufferedImage(1, 1, BufferedImage.TYPE_USHORT_565_RGB);
		img565.setRGB(0, 0, color);
		DataBuffer buff = img565.getData().getDataBuffer();
		return buff.getElem(0);
	}
}
