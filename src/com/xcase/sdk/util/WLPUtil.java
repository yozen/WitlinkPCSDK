package com.xcase.sdk.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.xcase.sdk.encoder.LZWEncoder;


public class WLPUtil {

	public static int CUBE_WIDTH = 240;
	public static int CUBE_HEIGHT = 240;
	public static int MAX_STR_LEN = 10;
	
	public static int RLC_TYPE = 0;
	public static int LZW_TYPE = 1;

	private static final int[] defaultFontSize = {150, 120, 80, 60, 55, 50, 40, 30, 30, 23};
	
	/**
	 * 字符串转换为wlp图片
	 * @param text——字符串
	 * @param path——wlp图片要保存的路径
	 * @return 生成的wlp图片路径
	 */
	public static String convertStringToWLP(String text, String path){
		try{
			File outFile = new File(path +text + ".png");
			boolean ret = convertStringToPNG(text, outFile);
			if(!ret){
				return null;
			}
			File wlpFile = new File(path + text + ".wlp");
			ret = convertPNGToWLP(outFile.getPath(), LZW_TYPE, wlpFile);
			if(!ret){
				return null;
			}
			return text + ".wlp";
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 字符串生成WLP格式的字节数组
	 * @param text——字符串
	 * @return WLP格式的字节数组
	 */
	public static String convertStringToWLPBytes(String text, int fontColor, int backgroundColor, String filePath){
		//获取font的样式应用在str上的整个矩形
		String fileName = text + "-" + Integer.toHexString(fontColor) + "-" + Integer.toHexString(backgroundColor) + ".wlp";
		File file = new File(filePath + fileName);
		if(file.exists()){
			return file.getAbsolutePath();
		}
		
		int strLen = text.length();
		if(strLen > MAX_STR_LEN || strLen <= 0){
			return null;
		}
		int fontSize = defaultFontSize[strLen-1];
		Font font = createVAGRoundedFont(Font.PLAIN, fontSize);
		Rectangle2D r = font.getStringBounds(text, new FontRenderContext(AffineTransform.getScaleInstance(1, 1), false, false));
		int unitHeight=(int)Math.floor(r.getHeight());	//获取单个字符的高度
		//获取整个str用了font样式的宽度这里用四舍五入后+1保证宽度绝对能容纳这个字符串作为图片的宽度
		int width = CUBE_WIDTH;  		//(int)Math.round(r.getWidth())+1;
		int height = CUBE_HEIGHT;		//unitHeight+3;						//把单个字符的高度+3保证高度绝对能容纳字符串作为图片的高度
		int xLoc = (CUBE_WIDTH - (int)(Math.round(r.getWidth())+1))/2;
		int yLoc = CUBE_HEIGHT/2 + unitHeight/4;
		//创建图片
		BufferedImage bufImg = new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
		Graphics g = bufImg.getGraphics();
		Color bgColor = new Color(backgroundColor);
		g.setColor(bgColor);
		g.fillRect(0, 0, width, height);	//先用白色填充整张图片,也就是背景
		Color fColor = new Color(fontColor);
		g.setColor(fColor);					//再换成黑色
		g.setFont(font);					//设置画笔字体
		g.drawString(text, xLoc, yLoc);		//画出字符串
		g.dispose();
		
		int rawRGB[][] = new int[height][width];
		for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
            	rawRGB[i][j] = bufImg.getRGB(j, i) & 0xFFFFFF;
            }
		}
		int bytesPerPixel = 1;
		byte[] encoded = new byte[width * height * 2];
		LZWEncoder lzwEncoder = new LZWEncoder();
		lzwEncoder.encodeLZW(rawRGB, width, height, bytesPerPixel, encoded);

		int headByte = 11;
	    int version = LZW_TYPE;
	    int usedColorCount = lzwEncoder.getUsedCCount();
	    int enbufsize = lzwEncoder.getOutputSize();
	    
	    int buffSize = headByte + usedColorCount * 2 + enbufsize;
		byte[] endcodeBuff = new byte[buffSize];
	    endcodeBuff[0] = 'W';
	    endcodeBuff[1] = 'L';
	    endcodeBuff[2] = 'P';
	    endcodeBuff[3] = 11;        //Head len
	    endcodeBuff[4] = (byte)(version&0xff);
	    endcodeBuff[5] = (byte)((version >> 8) & 0xff);
	    endcodeBuff[6] = (byte)(width & 0xFF);
	    endcodeBuff[7] = (byte)(height & 0xFF);
	    endcodeBuff[8] = (byte)usedColorCount;
	    endcodeBuff[9] = (byte)(enbufsize & 0xff);
	    endcodeBuff[10] = (byte)((enbufsize >> 8) & 0xff);
	    byte[] colorTable = new byte[usedColorCount * 2];
	    int j = 0;
	    for(int i=0;i<usedColorCount;i++){
	    	colorTable[j] = (byte)(lzwEncoder.getNewColorTable()[i]&0xFF);
	    	colorTable[j+1] = (byte)((lzwEncoder.getNewColorTable()[i]>>8)&0xFF);
	    	j += 2;
	    }
	    System.arraycopy(colorTable, 0, endcodeBuff, headByte, usedColorCount * 2);
	    System.arraycopy(encoded, 0, endcodeBuff, headByte+usedColorCount * 2, enbufsize);
	    
	    //保存WLP文件
	    try{
	    	file.createNewFile();
	    	FileOutputStream fop = new FileOutputStream(file);
		    fop.write(endcodeBuff, 0, buffSize);
		    fop.flush();
		    fop.close();
	    }catch (Exception e) {
			// TODO: handle exception
	    	e.printStackTrace();
		}
	    
		return file != null ? file.getAbsolutePath() : null;				
	}
	/**
	 * 字符串生成PNG图片，图片默认大小240*240，最大支持10个字符串长度
	 * @param str
	 * @param outFile
	 * @return
	 * @throws IOException
	 */
	public static boolean convertStringToPNG(String str, File outFile) throws IOException{
		//获取font的样式应用在str上的整个矩形
		int strLen = str.length();
		if(strLen > MAX_STR_LEN || strLen <= 0){
			return false;
		}
		int fontSize = defaultFontSize[strLen-1];
		
//		Font font = new Font("VAGRounded-Bold", Font.PLAIN, fontSize);
		Font font = createVAGRoundedFont(Font.PLAIN, fontSize);
		
		Rectangle2D r = font.getStringBounds(str, new FontRenderContext(AffineTransform.getScaleInstance(1, 1), false, false));
		int unitHeight=(int)Math.floor(r.getHeight());	//获取单个字符的高度
		//获取整个str用了font样式的宽度这里用四舍五入后+1保证宽度绝对能容纳这个字符串作为图片的宽度
		int width = CUBE_WIDTH;  		//(int)Math.round(r.getWidth())+1;
		int height = CUBE_HEIGHT;		//unitHeight+3;						//把单个字符的高度+3保证高度绝对能容纳字符串作为图片的高度
		int xLoc = (CUBE_WIDTH - (int)(Math.round(r.getWidth())+1))/2;
		int yLoc = CUBE_HEIGHT/2 + unitHeight/4;
		//创建图片
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
		Graphics g = image.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);//先用白色填充整张图片,也就是背景
		g.setColor(Color.black);		//再换成黑色
		g.setFont(font);				//设置画笔字体
		g.drawString(str, xLoc, yLoc);	//画出字符串
		g.dispose();

		ImageIO.write(image, "png", outFile);	//输出png图片
		
		return true;
	}
	
	/**
	 * 将PNG图片转化为WLP图片，并保存在resource目录下
	 * @param pngImage
	 * @param type
	 * @param outFileName
	 */
	public static boolean convertPNGToWLP(String pngImage, int type, File outFile){
		try{
			
			BufferedImage bufImg = ImageIO.read(new File(pngImage));
			int width = bufImg.getWidth();
			int height = bufImg.getHeight();
			
			int rawRGB[][] = new int[height][width];
			for (int i = 0; i < height; i++) {
	            for (int j = 0; j < width; j++) {
	            	rawRGB[i][j] = bufImg.getRGB(j, i) & 0xFFFFFF;
	            }
			}
			int bytesPerPixel = 1;
			byte[] encoded = new byte[width * height * 2];
			LZWEncoder lzwEncoder = new LZWEncoder();
			if(type == LZW_TYPE){
				lzwEncoder.encodeLZW(rawRGB, width, height, bytesPerPixel, encoded);
			}
			int headByte = 11;
		    int version = type;
		    int usedColorCount = lzwEncoder.getUsedCCount();
		    int enbufsize = lzwEncoder.getOutputSize();
			byte[] endcodeBuff = new byte[headByte + usedColorCount * 2 + enbufsize];
		    endcodeBuff[0] = 'W';
		    endcodeBuff[1] = 'L';
		    endcodeBuff[2] = 'P';
		    endcodeBuff[3] = 11;        //Head len
		    endcodeBuff[4] = (byte)(version&0xff);
		    endcodeBuff[5] = (byte)((version >> 8) & 0xff);
		    endcodeBuff[6] = (byte)(width & 0xFF);
		    endcodeBuff[7] = (byte)(height & 0xFF);
		    endcodeBuff[8] = (byte)usedColorCount;
		    endcodeBuff[9] = (byte)(enbufsize & 0xff);
		    endcodeBuff[10] = (byte)((enbufsize >> 8) & 0xff);
		    byte[] colorTable = new byte[usedColorCount * 2];
		    int j = 0;
		    for(int i=0;i<usedColorCount;i++){
		    	colorTable[j] = (byte)(lzwEncoder.getNewColorTable()[i]&0xFF);
		    	colorTable[j+1] = (byte)((lzwEncoder.getNewColorTable()[i]>>8)&0xFF);
		    	j += 2;
		    }
		    System.arraycopy(colorTable, 0, endcodeBuff, headByte, usedColorCount * 2);
		    System.arraycopy(encoded, 0, endcodeBuff, headByte+usedColorCount * 2, enbufsize);
		    
		    int buffSize = headByte + usedColorCount * 2 + enbufsize;

		    FileOutputStream fop = new FileOutputStream(outFile);
		    if(!outFile.exists()){
		    	outFile.createNewFile();
		    }
		    fop.write(endcodeBuff, 0, buffSize);
		    fop.flush();
		    fop.close();
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	public static Font createVAGRoundedFont(int style, int fontSize){
		try{
			File file = new File("resource/vagroun.ttf");
			if(file.exists()){
				FileInputStream aixing = new FileInputStream(file);
				Font dynamicFont = Font.createFont(Font.TRUETYPE_FONT, aixing);
				Font dynamicFontPt = dynamicFont.deriveFont((float)fontSize);
				aixing.close();
				return dynamicFontPt;
			}			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return new java.awt.Font("宋体", style, fontSize);
	}
	
}
