package com.xcase.sdk.util;

import java.util.List;

public class DataUtil {

	/**
	 * 字节数组�?6进制字符�?
	 * @param src
	 * @return
	 */
	public static String bytesToHexString(byte[] src,boolean reverse, int len){  
		StringBuilder stringBuilder = new StringBuilder("");  
	    if (src == null || len <= 0) {  
	        return null;  
	    }  
	    if(!reverse){
		    for (int i = 0; i < len; i++) {  
		        int v = src[i] & 0xFF;  
		        String hv = Integer.toHexString(v);  
		        if (hv.length() < 2) {  
		            stringBuilder.append(0);  
		        }  
		        stringBuilder.append(hv);  	//中间空格隔开
		    }  
	    }else{
	    	for (int i = len-1; i >= 0; i--) {  
		        int v = src[i] & 0xFF;  
		        String hv = Integer.toHexString(v);  
		        if (hv.length() < 2) {  
		            stringBuilder.append(0);  
		        }  
		        stringBuilder.append(hv+" ");  
		    }
	    }
	    
	    return stringBuilder.toString().toUpperCase();  
	} 
	
	/**
	 * 十六进制字符串转字节数组
	 * @param hexString
	 * @return
	 */
	public static byte[] hexStringToBytes(String hexString) {  
	    if (hexString == null || hexString.equals("")) {  
	        return null;  
	    }  
	    hexString = hexString.toUpperCase();  
	    int length = hexString.length() / 2;  
	    char[] hexChars = hexString.toCharArray();  
	    byte[] d = new byte[length];  
	    for (int i = 0; i < length; i++) {  
	        int pos = i * 2;  
	        d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));  
	    }  
	    return d;  
	}  
	
	private static byte charToByte(char c) {  
	    return (byte) "0123456789ABCDEF".indexOf(c);  
	}
	/**
	 * List<Byte>转字节数�?
	 * @param list
	 * @return
	 */
	public static byte[] arrayListToBytes(List<Byte> list){
		int size = list.size();
		byte[] ret = new byte[size];
		for(int i=0;i<size;i++){
			ret[i] = list.get(i);
		}
		
		return ret;
	}
	/**
	 * 将字节数组复制到List�?
	 * @param src
	 * @param srcStart	数组的开始位�?
	 * @param len 长度
	 * @param desc
	 */
	public static void copyBytesToList(byte[] src,int srcStart,List<Byte> desc,int len){
		if(src!=null && src.length>=len){
			for(int i=0;i<len;i++){
				desc.add(src[i+srcStart]);
			}
		}
	}
	
	/** 
	 * 字节转换为浮�?
	 *  
	 * @param b 字节（至�?个字节） 
	 * @param index �?��位置 
	 * @return 
	 */  
	public static float byte2float(byte[] b, int index) {    
	    int l;                                             
	    l = b[index + 0];                                  
	    l &= 0xff;                                         
	    l |= ((long) b[index + 1] << 8);                   
	    l &= 0xffff;                                       
	    l |= ((long) b[index + 2] << 16);                  
	    l &= 0xffffff;                                     
	    l |= ((long) b[index + 3] << 24); 
	    
	    return Float.intBitsToFloat(l);                    
	}
	
	/** 
	 * 浮点转换为字�?
	 *  
	 * @param f 
	 * @return 
	 */  
	public static byte[] float2byte(float f) {  
	      
	    // 把float转换为byte[]  
	    int fbit = Float.floatToIntBits(f);  
	      
	    byte[] b = new byte[4];    
	    for (int i = 0; i < 4; i++) {    
	        b[i] = (byte) (fbit >> (24 - i * 8));    
	    }   
	      
	    // 翻转数组  
	    int len = b.length;  
	    // 建立�?��与源数组元素类型相同的数�? 
	    byte[] dest = new byte[len];  
	    // 为了防止修改源数组，将源数组拷贝�?��副本  
	    System.arraycopy(b, 0, dest, 0, len);  
	    byte temp;  
	    // 将顺位第i个与倒数第i个交�? 
	    for (int i = 0; i < len / 2; ++i) {  
	        temp = dest[i];  
	        dest[i] = dest[len - i - 1];  
	        dest[len - i - 1] = temp;  
	    }  
	      
	    return dest;    
	}
	
	
	public static int[] intListTointArray(List<Integer> intList){
		if(intList == null){
			return null;
		}
		int len = intList.size();
		int[] ret = new int[len];
		for(int i=0;i<intList.size();i++){
			ret[i] = intList.get(i);
		}
		return ret;
	}
}
