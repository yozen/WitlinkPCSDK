package com.xcase.sdk.util;

import java.util.Timer;
import java.util.TimerTask;

public class XCaseDelayer{
	
	private static Timer timer;
	private static int timeoutFlag = -1;		//标志定时时间到
	
	private XCaseDelayer(){}

	public static int startTimer(int delay,int period){
		synchronized (XCaseDelayer.class) {
			if(timer == null){
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						// TODO TimerTask 判断变量是否不为1
						timeoutFlag = 1;
					}
				}, delay, period);
			}
		}
		return timeoutFlag;
	}
	
	public static void closeTimer(){
		synchronized (XCaseDelayer.class) {
			if(timer != null){
				timer.cancel();
				timer = null;
			}
			timeoutFlag = 0;
		}
	}
	
}
