package com.xcase.sdk.arrangement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by simon on 9/19/16.
 */
/**
 * GroupMatrix用来描述小块组的序列，有两种用途：
 * 1、自己创建一个GroupMatrix对象用于与cubeGroup做对比，判断两者是否匹配
 * 2、获取一个cubeGroup的GroupMatrix描述
 */
public class GroupMatrix {
	
	/** 用小块显示的内容名称表示矩阵，最常见的是图片名称 */
    public final String[][] contentNames;

    /** 存放小块旋转值的二维数组，元素取值：Orientation类的UP、RIGHT、DOWN、LEFT */
    public final int[][] orientations;

    /** 把下标对应的编号存入map */
    private Map<Point, String> nameMap;

    /** 以这个点的小块做标准，作用是给CubeGroup做同步，然后比较 */
    private Point standardPoint;

    /** 线性组合，横向或纵向 */
    public enum LinearType {
        HORIZONTAL,
        VERTICAL;
    }


    public GroupMatrix (String[][] contentNames, int[][] orientations) {
        this.contentNames = contentNames;
        this.orientations = correctOrientations(contentNames, orientations);
    }

    public GroupMatrix (String[] contentNames, LinearType linearType) {
        this(contentNames, null, linearType);
    }

    public GroupMatrix (String[] contentNames, int[] linearOrientations, LinearType linearType) {
        String[][] names = null;
        int[][] orientations = null;
        if (linearType == LinearType.HORIZONTAL) {
            if (contentNames != null) {
                names = new String[1][contentNames.length];
                names[0] = contentNames;
            }

            if (linearOrientations != null) {
                orientations = new int[1][linearOrientations.length];
                orientations[0] = linearOrientations;
            }
        } else {
            if (contentNames != null) {
                names = new String[contentNames.length][1];
                for (int i = 0; i < contentNames.length; i++) {
                    names[i][0] = contentNames[i];
                }
            }
            if (linearOrientations != null) {
                orientations = new int[linearOrientations.length][1];
                for (int i = 0; i < linearOrientations.length; i++) {
                    orientations[i][0] = linearOrientations[i];
                }
            }

        }
        this.contentNames = names;
        this.orientations = correctOrientations(names, orientations);
    }

    public List<Integer> optionOrientationsInCoordinate(int row, int col) {
        List<Integer> sides = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            int orientation = orientations[row][col];
            int side = orientation & (1 << i);
            if (side > 0) {
                sides.add(i);
            }
        }
        return sides;
    }


    public void generateNumberMap() {
        nameMap = new HashMap();
        List<Integer> rotates = null;
        //把matrix转成map，且找出可选rotates最少的小块
        for (int r = 0; r < contentNames.length; r++) {
            String[] rows = contentNames[r];
            for (int c = 0; c < rows.length; c++) {
                String name = rows[c];
                if (name != null) {
                    List<Integer> sides = optionOrientationsInCoordinate(r, c);
                    Point point = new Point(c, r);
                    //寻找最优的标准点
                    if (rotates == null || sides.size() < rotates.size()) {
                        rotates = sides;
                        standardPoint = point;
                    }
                    nameMap.put(point, name);
                }
            }
        }
    }

    private int[][] correctOrientations(String[][] names, int[][] orientations) {
        int[][] ots = null;
        if (orientations != null) {
            boolean isValid = false;
            if (names.length == orientations.length) {
                for (int i = 0; i < names.length; i++) {
                    if (names[i].length != orientations[i].length) {
                        break;
                    }
                }
                isValid = true;
            }
            if (isValid) {
                ots = orientations;
            }
        }
        if (ots == null) {
            ots = defaultOrientations(names);
        }
        return ots;
    }

    private int[][] defaultOrientations(String[][] names) {
        int[][] orientations = new int[names.length][names[0].length];
        for (int i = 0; i < orientations.length; i++) {
            for (int j = 0; j < orientations[i].length; j++) {
                orientations[i][j] = 1;
            }
        }
        return orientations;
    }


    public Point getStandardPoint() {
        if (standardPoint == null) {
            generateNumberMap();
        }
        return standardPoint;
    }

    public Map<Point, String> getNumberMap() {
        if (nameMap == null) {
            generateNumberMap();
        }
        return nameMap;
    }
	
	
	
//    /** 存放小块编号的二维数组，表示小块组的矩阵 */
//    public final int[][] numbers;
//    /** 存放小块旋转值的二维数组，元素取值：Orientation类的UP、RIGHT、DOWN、LEFT */
//    public final int[][] orientations;
//    /** 用小块显示图片的名称代替编号表示矩阵 */
//    public String[][] imageNames;
//
//    /** 把下标对应的编号存入map */
//    private Map<Point, Integer> numberMap;
//
//    /** 以这个点的小块做标准，作用是给CubeGroup做同步，然后比较 */
//    private Point standardPoint;
//
//    public enum LinearType {
//        HORIZONTAL,
//        VERTICAL;
//    }
//
//    public GroupMatrix (int[][] numbers, int[][] orientations) {
//        this.numbers = numbers;
//        this.orientations = correctOrientations(numbers, orientations);
//    }
//
//    public GroupMatrix (int[][] numbers) {
//        this(numbers, null);
//    }
//
//    public GroupMatrix (int[] linearNumbers, int[] linearOrientations, LinearType linearType) {
//        int[][] numbers = null;
//        int[][] orientations = null;
//        if (linearType == LinearType.HORIZONTAL) {
//            if (linearNumbers != null) {
//                numbers = new int[1][linearNumbers.length];
//                numbers[0] = linearNumbers;
//            }
//
//            if (linearOrientations != null) {
//                orientations = new int[1][linearOrientations.length];
//                orientations[0] = linearOrientations;
//            }
//        } else {
//            if (linearNumbers != null) {
//                numbers = new int[linearNumbers.length][1];
//                for (int i = 0; i < linearNumbers.length; i++) {
//                    numbers[i][0] = linearNumbers[i];
//                }
//            }
//            if (linearOrientations != null) {
//                orientations = new int[linearOrientations.length][1];
//                for (int i = 0; i < linearOrientations.length; i++) {
//                    orientations[i][0] = linearOrientations[i];
//                }
//            }
//
//        }
//        this.numbers = numbers;
//        this.orientations = correctOrientations(numbers, orientations);
//    }
//
//    public List<Integer> optionOrientationsInCoordinate(int row, int col) {
//        List<Integer> sides = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            int orientation = orientations[row][col];
//            int side = orientation & (1 << i);
//            if (side > 0) {
//                sides.add(i);
//            }
//        }
//        return sides;
//    }
//
//
//    public void generateNumberMap() {
//        numberMap = new HashMap();
//        List<Integer> rotates = null;
//        //把matrix转成map，且找出可选rotates最少的小块
//        for (int r = 0; r < numbers.length; r++) {
//            int[] rows = numbers[r];
//            for (int c = 0; c < rows.length; c++) {
//                int num = rows[c];
//                if (num > 0) {
//                    List<Integer> sides = optionOrientationsInCoordinate(r, c);
//                    Point point = new Point(c, r);
//                    if (rotates == null || rotates.size() > sides.size()) {
//                        rotates = sides;
//                        standardPoint = point;
//                    }
//                    numberMap.put(point, Integer.valueOf(num));
//                }
//            }
//        }
//    }
//
//    private int[][] correctOrientations(int[][] numbers, int[][] orientations) {
//        // TODO: 9/21/16 orientations有值但 抛异常
//        int[][] ots = null;
//        if (orientations != null) {
//            boolean isValid = false;
//            if (numbers.length == orientations.length) {
//                for (int i = 0; i < numbers.length; i++) {
//                    if (numbers[i].length != orientations[i].length) {
//                        break;
//                    }
//                }
//                isValid = true;
//            }
//            if (isValid) {
//                ots = orientations;
//            }
//        }
//        if (ots == null) {
//            ots = defaultOrientations(numbers);
//        }
//        return ots;
//    }
//
//    private int[][] defaultOrientations(int[][] numbers) {
//        int[][] orientations = new int[numbers.length][numbers[0].length];
//        for (int i = 0; i < orientations.length; i++) {
//            for (int j = 0; j < orientations[i].length; j++) {
//                orientations[i][j] = 1;
//            }
//        }
//        return orientations;
//    }
//
//
//    public Point getStandardPoint() {
//        if (standardPoint == null) {
//            generateNumberMap();
//        }
//        return standardPoint;
//    }
//
//    public Map<Point, Integer> getNumberMap() {
//        if (numberMap == null) {
//            generateNumberMap();
//        }
//        return numberMap;
//    }
}
