package com.xcase.sdk.arrangement;

/**
 * Created by simon on 8/8/16.
 */
public class Point {
    public int x = 0;
    public int y = 0;

    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public Point() {
        this(0, 0);
    }

    public String toString() {
        return String.format("(%d,%d)",x,y);
    }


    public int hashCode() {
        return x * 100 + y;
    }

    public boolean equals(Object other) {
        if(this == other)
            return true;
        if(other == null)
            return false;
        if( !(other instanceof Point))
            return false;

        final Point c = (Point)other;

        return c.x == this.x && c.y == this.y;
    }

//    public String keyFromCoordinate
}


