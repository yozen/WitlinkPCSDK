package com.xcase.sdk.arrangement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.core.BleCube;
import com.xcase.sdk.xinterface.XCaseManager;


/**
 * Created by simon on 8/7/16.
 */

/*
说明：
把相连接的多个小块、单独的一个小块称作一个组，CubeGroup即是表示小块组的对象。
每个小块都有一个Group，如果两个（或多个）小块相连，则这些小块有同一个Group；
*/

public class CubeGroup {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(CubeGroup.class);
	
	//小块的四个边，上右下左对应{0, 1, 2, 3}
    private static final Integer[] sides = new Integer[]{0, 1, 2, 3};

    /** 当两个小块相碰或分开时，正常情况下这两个小块会“相继”触发onCharacteristicChanged事件，side值改变；
    反过来说，收到两个小块的side值改变后说明这两个小块连接状态发生改变（接触或分开）。
    activityCubes的作用是收集所有激活的小块，即发生onCharacteristicChanged之后，待处理的小块。PS：注意是static的*/
    private static final List<BleCube> activityCubes = new CopyOnWriteArrayList<>();
    //小块组的ID，用时间表示
    private final UUID groupId = UUID.randomUUID();
    /** 一个Group所拥有的小块的集合，把小块组的平面视为一个平面坐标系，小块的坐标（Point）表面了小块在Group中所处的位置*/
    private final List<BleCube> cubes = new CopyOnWriteArrayList<>();

    public boolean equals(Object other) {
        if(this == other)
            return true;
        if(other == null)
            return false;
        if( !(other instanceof CubeGroup))
            return false;

        final CubeGroup group2 = (CubeGroup)other;
        return (groupId.equals(group2.groupId));
    }

    public String toString() {
        String str = new String();
        for (int i = 0; i < cubes.size(); i++) {
            BleCube cube = cubes.get(i);
            str = str + cube.point.toString() + " orientation:" + cube.rotate + " content:" + cube.contentName;
        }
        return str;
    }

    public static void printGroups() {
        CubeGroup[] groups = XCaseManager.getInstance().getGroups();
        for (CubeGroup group : groups){
            LOGGER.debug("group " + group.groupId);
            LOGGER.debug(group.toString());
        }
    }

    private static void removeActivityCube(BleCube cube) {
        activityCubes.remove(cube);
        cube.changedRecord.clear();
    }

    //一个cube的side状态改变，执行该方法
    public synchronized static void handleCubeSideStateChanged(BleCube cube, Integer changedValue) {
        if (activityCubes.contains(cube)) {
            activityCubes.remove(cube);
        }
        activityCubes.add(0, cube);
        if (cube.changedRecord.size() == 0 || activityCubes.size() < 2) {
            return;
        }
        printGroups();
        //遍历activityCubes，寻找与参数cube（合并or分开）匹配的那个小块，如果没有，则把当前cube加入activityCubes
        for (BleCube aCube : activityCubes) {
            if (aCube == cube) continue;
            for (Integer side : aCube.changedRecord) {
                if (side > 0 && changedValue > 0) {
                    int errorCount = tryMergeAtCubes(aCube, side, cube, changedValue);
                    if (errorCount == 0) {
                        LOGGER.debug("--------before------");
                        printGroups();
                        mergeGroups(aCube.group, cube.group);
                        cube.actionCallback.onMerge(aCube.group,aCube,cube);
                        removeActivityCube(cube);
                        removeActivityCube(aCube);
                        LOGGER.debug("--------after merge------");
                        printGroups();
                        return;
                    }
                } else if (side < 0 && changedValue < 0) {
                    LOGGER.info("begin tryDivideAtCubes...");
                    if (tryDivideAtCubes(aCube, cube)) {
                        LOGGER.info("end tryDivideAtCubes...");
                        aCube.changedRecord.remove(side);
                        cube.changedRecord.remove(changedValue);
                        cube.actionCallback.onDivide(aCube,cube);
                        printGroups();
                        removeActivityCube(cube);
                        removeActivityCube(aCube);
                        return;
                    }
                }
            }
        }
    }

    public CubeGroup() {
        this(null);
    }

    public CubeGroup(BleCube cube) {
        super();
        if (cube != null) {
            addCube(cube);
        }
    }

    private void addCube(BleCube cube) {
        cubes.add(cube);
        cube.group = this;
    }

    private void addCubes(List<BleCube> cubes) {
        for (BleCube cube : cubes) {
            addCube(cube);
        }
    }

    private static boolean isNeighbor(BleCube cube1, BleCube cube2) {
        if (cube1.group != cube2.group) return false;
        int x = Math.abs(cube1.point.x - cube2.point.x);
        int y = Math.abs(cube1.point.y - cube2.point.y);

        if (x == 0 && y == 1 || x == 1 && y == 0) {
            return true;
        }

        if (x == 0 && y == 0) {
            return true;
        }

        return false;
    }
//    private static BleCube cube;
    //从cube1的group中分离cube2
    private static List<BleCube> divideAtCubes(BleCube cube1, BleCube cube2, List<BleCube> allCubes) {
        List<BleCube> newCubes = new ArrayList<>();
        List<BleCube> cubes = new CopyOnWriteArrayList<>(allCubes);
        cubes.remove(cube2);
        recursiveNeighborsFillMap(cube1,cubes,newCubes);
        return newCubes;
    }

    private static boolean tryDivideAtCubes(BleCube cube1, BleCube cube2) {
        if (!isNeighbor(cube1, cube2)) {
            return false;
        }
        List<BleCube> cubes = new ArrayList<>(cube1.group.cubes);

        List<BleCube> groupCubes = divideAtCubes(cube1, cube2 , cubes);
        cubes.removeAll(groupCubes);

        CubeGroup group1 = new CubeGroup();
        group1.addCubes(groupCubes);
        CubeGroup group2 = new CubeGroup();
        group2.addCubes(cubes);

        transGroupAtCube(cube1, new Point(0,0), 0);
        transGroupAtCube(cube2, new Point(0,0), 0);

        return true;
    }


    public static CubeGroup mergeGroups(CubeGroup group1, CubeGroup group2) {
        CubeGroup group = new CubeGroup();
        group.addCubes(group1.cubes);
        group.addCubes(group2.cubes);
        return group;
    }

    //两个小块发生碰撞，以cube1为标准，合并cube2的group
    public static int tryMergeAtCubes(BleCube cube1, int side1, BleCube cube2, int side2) {
        if (cube1.group == cube2.group || side1 == 0 || side2 == 0 || cube1 == cube2) {
            return -1;
        }

        int oSide1 = (int) (Math.log(side1) / Math.log(2));
        int oSide2 = (int) (Math.log(side2) / Math.log(2));

        transGroupAtCube(cube1, new Point(0,0), 0);

        //cube2相对于cube1的roate
        int rotate;
        if ((oSide2 - oSide1) % 2 == 0) {
            rotate = adjustOrientation(oSide2 - oSide1 - 2);
        } else {
            rotate = adjustOrientation(oSide2 - oSide1);
        }

        //cube2相对于当前cube1.group的roate
//        int newRotate = adjustOrientation(cube1.rotate + rotate);

        //算出cube1的side相邻位置的坐标
        Point newPoint = new Point(cube1.point.x, cube1.point.y);
        if (oSide1 == 0) {
            newPoint.y += 1;
        } else if (oSide1 == 1) {
            newPoint.x += 1;
        } else if (oSide1 == 2) {
            newPoint.y -= 1;
        } else if (oSide1 == 3) {
            newPoint.x -= 1;
        };

        LOGGER.debug("s1:" + oSide1 + " s2:" + oSide2  + " rotate:"+rotate);

        //当前group尝试合并cube2的group
        transGroupAtCube(cube2,newPoint,rotate);
        int errorCount = cube1.group.checkConflictForMerge(cube2.group);

        return errorCount;
    }



    private static void recursiveNeighborsFillMap(BleCube cube, List<BleCube> originalCubes, List<BleCube> newCubes) {
        newCubes.add(cube);
        Point point = cube.point;
        originalCubes.remove(cube);

        Point left = new Point(point.x - 1,point.y);
        Point right = new Point(point.x + 1,point.y);
        Point up = new Point(point.x,point.y + 1);
        Point down = new Point(point.x,point.y - 1);

        Point[] points = new Point[]{left,right,up,down};
        for (BleCube oCube : originalCubes) {
            for (Point neighborPoint : points) {
                if (oCube.point.equals(neighborPoint) && !newCubes.contains(oCube)) {
                    recursiveNeighborsFillMap(oCube,originalCubes,newCubes);
                }
            }
        }
    }

    private static int adjustOrientation(int orientation) {
        while (orientation < 0) {
            orientation += sides.length;
        }
        while (orientation > sides.length - 1) {
            orientation -= sides.length;
        }
        return orientation;
    }

    private static Point pointRotate(Point point, int rotate) {
        Point p = new Point(point.x, point.y);
        switch (rotate) {
            case 1:
                p.x = point.y;
                p.y = -point.x;
                break;
            case 2:
                p.x = -point.x;
                p.y = -point.y;
                break;
            case 3:
                p.x = -point.y;
                p.y = point.x;
                break;
        }
        return p;
    }

    //改变整个group的坐标系，1.把cube作为原点；2.旋转；3.设定偏移;4.同步到当前group。返回两个group冲突的point的数量errorCount，理论上errorCount是0才能合并
    private synchronized static void transGroupAtCube(BleCube theCube, Point newPoint, int newRotate) {
        int groupRotate = adjustOrientation(newRotate - theCube.rotate);
        Point rotatePoint = pointRotate(theCube.point, groupRotate);
        Point offset = new Point(newPoint.x - rotatePoint.x, newPoint.y - rotatePoint.y);

        if (theCube.point.equals(newPoint) && groupRotate == 0) return;

        for (BleCube cube : theCube.group.cubes) {
            Point point = cube.point;
            int temp = point.x;
            //group旋转groupRotate * 90度
            switch (groupRotate) {
                case 1:
                    point.x = point.y;
                    point.y = -temp;
                    break;
                case 2:
                    point.x = -point.x;
                    point.y = -point.y;
                    break;
                case 3:
                    point.x = -point.y;
                    point.y = temp;
                    break;
            }

            point.x += offset.x;
            point.y += offset.y;

            cube.rotate = adjustOrientation(cube.rotate + groupRotate);
        }
    }


    public int checkConflictForMerge(CubeGroup group) {
        int errorCount = 0;
        for (BleCube myCube : cubes) {
            for (BleCube cube : group.cubes) {
                if (myCube.point.equals(cube.point)) {
                    errorCount ++;
                }
            }
        }
        return errorCount;
    }

    /**
     *获取当前组包含的小块
     * @return 返回包含的小块数组
     */
    public BleCube[] getCubes() {
        BleCube[] myCubes = new BleCube[cubes.size()];
        return cubes.toArray(myCubes);
    }

    private BleCube getCube(String contentName) {
        for (BleCube cube : cubes) {
            if (contentName.equals(cube.contentName)) {
                return cube;
            }
        }
        return null;
    }

    private boolean excuteCompareMatrix(GroupMatrix matrix) {
        Map<Point, String> map = matrix.getNumberMap();
        for (BleCube cube : cubes) {
            String name = map.get(cube.point);
            if (name == null) {
                return false;
            }
            if (!name.equals(cube.contentName) && name.equals("*")) {
                return false;
            }
            int orientation = matrix.orientations[cube.point.y][cube.point.x];
            if ((orientation & (1 << cube.rotate)) == 0) {
                return false;
            }
        }

        return true;
    }

    public boolean compareMatrix(GroupMatrix matrix) {
        if (matrix.getNumberMap().size() != cubes.size()) return false;
        Point thePoint = matrix.getStandardPoint();
        String name = matrix.getNumberMap().get(thePoint);
        BleCube cube = getCube(name);
        if (cube == null) return false;

        List<Integer> rotates = matrix.optionOrientationsInCoordinate(thePoint.y,thePoint.x);
        for (int i = 0; i < rotates.size(); i++) {
            int rotate = rotates.get(i);
            transGroupAtCube(cube, thePoint, rotate);
            if (excuteCompareMatrix(matrix)) {
                return true;
            }
        }
        return false;
    }
	
	
	
	
//	//小块的四个边，上右下左对应{0, 1, 2, 3}
//    static final Integer[] sides = new Integer[]{0, 1, 2, 3};
//    /*当两个小块相碰或分开时，正常情况下这两个小块会“相继”触发onCharacteristicChanged事件，side值改变；
//                 反过来说，收到两个小块的side值改变后说明这两个小块连接状态发生改变（接触或分开）。
//      activityCubes的作用是收集所有激活的小块，即发生onCharacteristicChanged之后，待处理的小块。PS：注意是static的*/
//    public static final List<BleCube> activityCubes = new CopyOnWriteArrayList<>();
//    //小块组的ID，用时间表示
//    public final UUID groupId = UUID.randomUUID();
//    /*（重要）一个Group所拥有的小块的集合，把小块组的平面视为一个平面坐标系，小块的坐标（Point）表面了小块在Group中所处的位置*/
//    public final List<BleCube> cubes = new CopyOnWriteArrayList<>();
//
//    public boolean equals(Object other) {
//        if(this == other)
//            return true;
//        if(other == null)
//            return false;
//        if( !(other instanceof CubeGroup))
//            return false;
//
//        final CubeGroup group2 = (CubeGroup)other;
//        return (groupId.equals(group2.groupId));
//    }
//
//    public String toString() {
//        String str = new String();
//        for (int i = 0; i < cubes.size(); i++) {
//            BleCube cube = cubes.get(i);
//            str = str + cube.point.toString() + " orientation:" + cube.rotate + " " + cube.address + "\n";
//        }
//        return str;
//    }
//
//    public static void printGroups() {
//        List<CubeGroup> groups = XCaseManager.getInstance().allGroups();
//        for (CubeGroup group : groups){
//            LOGGER.debug("group " + group.groupId);
//            LOGGER.debug(group.toString());
//        }
//    }
//
//    private static void removeActivityCube(BleCube cube) {
//        activityCubes.remove(cube);
//        cube.changedRecord.clear();
//    }
//
//    //一个cube的side状态改变，执行该方法
//    public synchronized static void handleCubeSideStateChanged(BleCube cube, Integer changedValue) {
//        if (activityCubes.contains(cube)) {
//            activityCubes.remove(cube);
//        }
//        activityCubes.add(0, cube);
//        if (cube.changedRecord.size() == 0 || activityCubes.size() < 2) {
//            return;
//        }
//        printGroups();
//        //遍历activityCubes，寻找与参数cube（合并or分开）匹配的那个小块，如果没有，则把当前cube加入activityCubes
//        for (BleCube aCube : activityCubes) {
//            if (aCube == cube) continue;
//            for (Integer side : aCube.changedRecord) {
//                if (side > 0 && changedValue > 0) {
//                    int errorCount = tryMergeAtCubes(aCube, side, cube, changedValue);
//                    if (errorCount == 0) {
//                    	LOGGER.debug("--------before------");
//                        printGroups();
//                        mergeGroups(aCube.group, cube.group);
//                        cube.actionCallback.onMerge(aCube.group,aCube,cube);
//                        removeActivityCube(cube);
//                        removeActivityCube(aCube);
//                        LOGGER.debug("--------after merge------");
//                        printGroups();
//                        return;
//                    }
//                } else if (side < 0 && changedValue < 0) {
//                	LOGGER.debug("begin tryDivideAtCubes...");
//                    if (tryDivideAtCubes(aCube, cube)) {
//                    	LOGGER.debug("end tryDivideAtCubes...");
//                        aCube.changedRecord.remove(side);
//                        cube.changedRecord.remove(changedValue);
//                        cube.actionCallback.onDivide(aCube,cube);
//                        printGroups();
//                        removeActivityCube(cube);
//                        removeActivityCube(aCube);
//                        return;
//                    }
//                }
//            }
//        }
//    }
//
//    public CubeGroup() {
//        this(null);
//    }
//
//    public CubeGroup(BleCube cube) {
//        super();
//        if (cube != null) {
//            addCube(cube);
//        }
//    }
//
//    private void addCube(BleCube cube) {
//        cubes.add(cube);
//        cube.group = this;
//    }
//
//    private void addCubes(List<BleCube> cubes) {
//        for (BleCube cube : cubes) {
//            addCube(cube);
//        }
//    }
//
//    private static boolean isNeighbor(BleCube cube1, BleCube cube2) {
//        if (cube1.group != cube2.group) return false;
//        int x = Math.abs(cube1.point.x - cube2.point.x);
//        int y = Math.abs(cube1.point.y - cube2.point.y);
//
//        if (x == 0 && y == 1 || x == 1 && y == 0) {
//            return true;
//        }
//
//        if (x == 0 && y == 0) {
//            return true;
//        }
//
//        return false;
//    }
////    private static BleCube cube;
//    //从cube1的group中分离cube2
//    private static List<BleCube> divideAtCubes(BleCube cube1, BleCube cube2, List<BleCube> allCubes) {
//        List<BleCube> newCubes = new ArrayList<>();
//        List<BleCube> cubes = new CopyOnWriteArrayList<>(allCubes);
//        cubes.remove(cube2);
//        recursiveNeighborsFillMap(cube1,cubes,newCubes);
//        return newCubes;
//    }
//
//    private static boolean tryDivideAtCubes(BleCube cube1, BleCube cube2) {
//        if (!isNeighbor(cube1, cube2)) {
//            return false;
//        }
//        List<BleCube> cubes = new ArrayList<>(cube1.group.cubes);
//
//        List<BleCube> groupCubes = divideAtCubes(cube1, cube2 , cubes);
//        cubes.removeAll(groupCubes);
//
//        CubeGroup group1 = new CubeGroup();
//        group1.addCubes(groupCubes);
//        CubeGroup group2 = new CubeGroup();
//        group2.addCubes(cubes);
//
//        transGroupAtCube(cube1, new Point(0,0), 0);
//        transGroupAtCube(cube2, new Point(0,0), 0);
//
//        return true;
//    }
//
//
//    public static CubeGroup mergeGroups(CubeGroup group1, CubeGroup group2) {
//        CubeGroup group = new CubeGroup();
//        group.addCubes(group1.cubes);
//        group.addCubes(group2.cubes);
//        return group;
//    }
//
//    //两个小块发生碰撞，以cube1为标准，合并cube2的group
//    public static int tryMergeAtCubes(BleCube cube1, int side1, BleCube cube2, int side2) {
//        if (cube1.group == cube2.group || side1 == 0 || side2 == 0 || cube1 == cube2) {
//            return -1;
//        }
//
//        int oSide1 = (int) (Math.log(side1) / Math.log(2));
//        int oSide2 = (int) (Math.log(side2) / Math.log(2));
//
//        transGroupAtCube(cube1, new Point(0,0), 0);
//
//        //cube2相对于cube1的roate
//        int rotate;
//        if ((oSide2 - oSide1) % 2 == 0) {
//            rotate = adjustOrientation(oSide2 - oSide1 - 2);
//        } else {
//            rotate = adjustOrientation(oSide2 - oSide1);
//        }
//
//        //cube2相对于当前cube1.group的roate
////        int newRotate = adjustOrientation(cube1.rotate + rotate);
//
//        //算出cube1的side相邻位置的坐标
//        Point newPoint = new Point(cube1.point.x, cube1.point.y);
//        if (oSide1 == 0) {
//            newPoint.y += 1;
//        } else if (oSide1 == 1) {
//            newPoint.x += 1;
//        } else if (oSide1 == 2) {
//            newPoint.y -= 1;
//        } else if (oSide1 == 3) {
//            newPoint.x -= 1;
//        };
//
//        LOGGER.debug("s1:" + oSide1 + " s2:" + oSide2  + " rotate:"+rotate);
//
//        //当前group尝试合并cube2的group
//        transGroupAtCube(cube2,newPoint,rotate);
//        int errorCount = cube1.group.checkConflictForMerge(cube2.group);
//
//        return errorCount;
//    }
//
//
//
//    private static void recursiveNeighborsFillMap(BleCube cube, List<BleCube> originalCubes, List<BleCube> newCubes) {
//        newCubes.add(cube);
//        Point point = cube.point;
//        originalCubes.remove(cube);
//
//        Point left = new Point(point.x - 1,point.y);
//        Point right = new Point(point.x + 1,point.y);
//        Point up = new Point(point.x,point.y + 1);
//        Point down = new Point(point.x,point.y - 1);
//
//        Point[] points = new Point[]{left,right,up,down};
//        for (BleCube oCube : originalCubes) {
//            for (Point neighborPoint : points) {
//                if (oCube.point.equals(neighborPoint) && !newCubes.contains(oCube)) {
//                    recursiveNeighborsFillMap(oCube,originalCubes,newCubes);
//                }
//            }
//        }
//    }
//
//    private static int adjustOrientation(int orientation) {
//        while (orientation < 0) {
//            orientation += sides.length;
//        }
//        while (orientation > sides.length - 1) {
//            orientation -= sides.length;
//        }
//        return orientation;
//    }
//
//    private static Point pointRotate(Point point, int rotate) {
//        Point p = new Point(point.x, point.y);
//        switch (rotate) {
//            case 1:
//                p.x = point.y;
//                p.y = -point.x;
//                break;
//            case 2:
//                p.x = -point.x;
//                p.y = -point.y;
//                break;
//            case 3:
//                p.x = -point.y;
//                p.y = point.x;
//                break;
//        }
//        return p;
//    }
//
//    //改变整个group的坐标系，1.把cube作为原点；2.旋转；3.设定偏移;4.同步到当前group。返回两个group冲突的point的数量errorCount，理论上errorCount是0才能合并
//    private synchronized static void transGroupAtCube(BleCube theCube, Point newPoint, int newRotate) {
//        int groupRotate = adjustOrientation(newRotate - theCube.rotate);
//        Point rotatePoint = pointRotate(theCube.point, groupRotate);
//        Point offset = new Point(newPoint.x - rotatePoint.x, newPoint.y - rotatePoint.y);
//
//        if (theCube.point.equals(newPoint) && groupRotate == 0) return;
//
//        for (BleCube cube : theCube.group.cubes) {
//            Point point = cube.point;
//            int temp = point.x;
//            //group旋转groupRotate * 90度
//            switch (groupRotate) {
//                case 1:
//                    point.x = point.y;
//                    point.y = -temp;
//                    break;
//                case 2:
//                    point.x = -point.x;
//                    point.y = -point.y;
//                    break;
//                case 3:
//                    point.x = -point.y;
//                    point.y = temp;
//                    break;
//            }
//
//            point.x += offset.x;
//            point.y += offset.y;
//
//            cube.rotate = adjustOrientation(cube.rotate + groupRotate);
//        }
//    }
//
//
//    public int checkConflictForMerge(CubeGroup group) {
//        int errorCount = 0;
//        for (BleCube myCube : cubes) {
//            for (BleCube cube : group.cubes) {
//                if (myCube.point.equals(cube.point)) {
//                    errorCount ++;
//                }
//            }
//        }
//        return errorCount;
//    }
//
//    private BleCube findCube(int number) {
//        for (BleCube cube : cubes) {
//            if (cube.getCubeID() == number) {
//                return cube;
//            }
//        }
//        return null;
//    }
//
//    private boolean excuteCompareMatrix(GroupMatrix matrix) {
//        Map<Point, Integer> map = matrix.getNumberMap();
//        for (BleCube cube : cubes) {
//            Integer numValue = map.get(cube.point);
//            if (numValue == null) {
//                return false;
//            }
//            int num = numValue.intValue();
//            if (num != cube.getCubeID() && num != -1) {
//                return false;
//            }
//            int orientation = matrix.orientations[cube.point.y][cube.point.x];
//            if ((orientation & (1 << cube.rotate)) == 0) {
//                return false;
//            }
//        }
//
//        return true;
//    }
//
//    public boolean compareMatrix(GroupMatrix matrix) {
//        if (matrix.getNumberMap().size() != cubes.size()) return false;
//        Point thePoint = matrix.getStandardPoint();
//        int num = matrix.getNumberMap().get(thePoint);
//        BleCube cube = findCube(num);
//        if (cube == null) return false;
//
//        List<Integer> rotates = matrix.optionOrientationsInCoordinate(thePoint.y,thePoint.x);
//        for (int i = 0; i < rotates.size(); i++) {
//            int rotate = rotates.get(i);
//            transGroupAtCube(cube, thePoint, rotate);
//            if (excuteCompareMatrix(matrix)) {
//                return true;
//            }
//        }
//        return false;
//    }    
}
