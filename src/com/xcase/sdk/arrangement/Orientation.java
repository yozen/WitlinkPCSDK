package com.xcase.sdk.arrangement;

/**
 * Created by simon on 9/26/16.
 */
public class Orientation {
    public static final int OBVERSE = 0x02;
    public static final int REVERSE = 0x01;
    public static final int UPRIGHT_UP = 0x20;
    public static final int UPRIGHT_RIGHT = 0x04;
    public static final int UPRIGHT_DOWN = 0x04;
    public static final int UPRIGHT_LEFT = 0x08;
    
//    private static final int OBVERSE_VALUE = 0x02;
//    private static final int REVERSE_VALUE = 0x01;
//    private static final int UPRIGHT_UP_VALUE = 0x20;
//    private static final int UPRIGHT_RIGHT_VALUE = 0x04;
//    private static final int UPRIGHT_DOWN_VALUE = 0x10;
//    private static final int UPRIGHT_LEFT_VALUE = 0x08;
//    
//    public static int getOrientation(int orientation){
//    	int ret = -1;
//    	switch(orientation){
//    	case OBVERSE_VALUE:
//    		ret = OBVERSE;
//    		break;
//    	case REVERSE_VALUE:
//    		ret = REVERSE;
//    		break;
//    	case UPRIGHT_UP_VALUE:
//    		ret = UPRIGHT_UP;
//    		break;
//    	case UPRIGHT_RIGHT_VALUE:
//    		ret = UPRIGHT_RIGHT;
//    		break;
//    	case UPRIGHT_DOWN_VALUE:
//    		ret = UPRIGHT_DOWN;
//    		break;
//    	case UPRIGHT_LEFT_VALUE:
//    		ret = UPRIGHT_LEFT;
//    		break;
//    	default:
//    		break;
//    	}
//    	return ret;
//    }
}
