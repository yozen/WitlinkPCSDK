package com.xcase.sdk.arrangement;

/**
 * Created by simon on 9/20/16.
 */

/** 小块物理正上方指向的Group方向 */
public class Rotate {
    /** “小块物理正上方”指向“Group的上边”，说明小块在Group中没有旋转，UP为默认值 */
    public static final int UP = 1;
    /** “小块物理正上方”指向“Group的右边”，说明小块在Group中旋转90度*/
    public static final int RIGHT = 1 << 1;
    /** “小块物理正上方”指向“Group的下边”，说明小块在Group中旋转180度*/
    public static final int DOWN = 1 << 2;
    /** “小块物理正上方”指向“Group的左边”，说明小块在Group中旋转270度*/
    public static final int LEFT = 1 << 3;
   
}
