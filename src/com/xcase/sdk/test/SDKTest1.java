package com.xcase.sdk.test;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.arrangement.CubeGroup;
import com.xcase.sdk.arrangement.GroupMatrix;
import com.xcase.sdk.arrangement.Rotate;
import com.xcase.sdk.core.BleCube;
import com.xcase.sdk.xinterface.XCaseManager;
import com.xcase.sdk.xinterface.XCaseProtocol.ActionCallback;
import com.xcase.sdk.xinterface.XCaseProtocol.ConnectCallback;
import com.xcase.sdk.xinterface.XCaseProtocol.SendImageCallback;

public class SDKTest1 {

	private final static Logger LOGGER = LoggerFactory.getLogger(SDKTest1.class);
	
	private static String loggerPath = "resource/log4j.properties";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		// TODO main		
		PropertyConfigurator.configure(loggerPath); 
		
		String file01 = "resource/icon.wlp";
		String file02 = "resource/ball.wlp";
		String file03 = "resource/icon.wlp";
		String file04 = "resource/book.wlp";
		String file05 = "resource/chair.wlp";
		String file06 = "resource/icon.wlp";
		String file07 = "resource/icon.wlp";
		String file08 = "resource/icon.wlp";
		final String[] imageFiles = new String[]{file01,file02,file03,file04,file05,file06,file07,file08,file01,file02,file03,file04,file05,file06,file07,file08};
		
		LOGGER.debug("XCASE SDK TEST CENTRAL");
		final XCaseManager xcaseManager = XCaseManager.getInstance();
		final List<BleCube> connCubes = new ArrayList<BleCube>();
		if(xcaseManager.initialize() != 1){
			//初始化失败
			LOGGER.info("initialize failed");
			System.exit(0);
		}
		
//		xcaseManager.requestCubes(8, new ConnectCallback() {
//			
//			@Override
//			public void onDisconnect(BleCube cube) {
//				// TODO onDisconnect
//				if(cube != null){
//					LOGGER.info("Cube " + cube.getCubeID() + " disconnected!!!");
//				}
//			}
//			
//			@Override
//			public void onConnect(List<BleCube> connectedCubes) {
//				// TODO onConnect
//				connCubes.clear();
//				for(BleCube cube : connectedCubes){
//					connCubes.add(cube);
//				}
//				LOGGER.info("connected cube num=" + connectedCubes.size());
//			}
//		});
		xcaseManager.setActionCallback(new ActionCallback() {
			
			@Override
			public void onTurn(BleCube cube, int lastOrientation, int currentOrientation) {
				// TODO onTurn
				LOGGER.info("Cube " + cube.getCubeID() + " lastOrientation=" + lastOrientation 
						+ " currentOrientation=" + currentOrientation);
			}
			
			@Override
			public void onShake(BleCube cube, int times) {
				// TODO onShake
				LOGGER.info("Cube " + cube.getCubeID() + " shake " + times + " times");
			}
			
			@Override
			public void onMerge(CubeGroup group, BleCube cube1, BleCube cube2) {
				// TODO onMerge
				LOGGER.info("Merge! " + cube1.getCubeID() + " and " +  cube2.getCubeID() + " merge");
				CubeGroup.printGroups();
				int[] ots = new int[]{Rotate.UP | Rotate.LEFT, Rotate.UP, Rotate.UP};
				if(group.getCubes().length >= imageFiles.length){
					GroupMatrix matrix = new GroupMatrix(imageFiles, null, GroupMatrix.LinearType.HORIZONTAL);
	            	boolean result = xcaseManager.groupCompareMatrix(group, matrix);
	            	LOGGER.info("Merge Result=" + result);
	            	if(result){
	            		for (BleCube bleCube : connCubes) {
	    					xcaseManager.twinkleCube(bleCube, 3);
	    				}
	            	}
				}
			}
			
			@Override
			public void onDivide(BleCube cube1, BleCube cube2) {
				// TODO onDivide
				LOGGER.info("Divide! " + cube1.getCubeID() + " and " + cube2.getCubeID() + " divide");
			}
		});
		
		Scanner scanner = new Scanner(System.in);
		while(true){
			LOGGER.info("please input command: ");
			String str = scanner.next();
			if(str.equals("1")){
				//启动扫描
				xcaseManager.requestCubes(16, new ConnectCallback() {
					
					@Override
					public void onDisconnect(BleCube cube) {
						// TODO Auto-generated method stub
						if(cube != null){
							LOGGER.info("Cube " + cube.getCubeID() + " disconnected!!!");
						}
						connCubes.remove(cube);
					}
					
					@Override
					public void onConnect(List<BleCube> connectedCubes) {
						// TODO Auto-generated method stub
						connCubes.clear();
						for(BleCube cube : connectedCubes){
							connCubes.add(cube);
						}
						LOGGER.info("connected cube num=" + connectedCubes.size());
					}
				});
			}else if(str.equals("2")){
				//断开连接
				xcaseManager.close();
			}else if(str.equals("3")){
				//发送图片
				if(connCubes != null){
					xcaseManager.displayCubesImages(xcaseManager.getCubes(), imageFiles, new SendImageCallback() {
						
						@Override
						public void onSendImageSuccess() {
							// TODO Auto-generated method stub
							LOGGER.info("image send success");
						}
						
						@Override
						public void onSendImageFail(BleCube cube, int errorCode) {
							// TODO Auto-generated method stub
							LOGGER.info("image send failed");
						}
					});
				}
			}else if(str.equals("4")){
				//发送图片
				if(connCubes != null){
					
				}
			}else if(str.equals("5")){
				//画边框
				for (BleCube bleCube : connCubes) {
					xcaseManager.drawCubeFrame(bleCube, Color.gray.getRGB(), 8);
				}
			}else if(str.equals("6")){
				//设置背景色
				for (BleCube bleCube : connCubes) {
					xcaseManager.setCubeBackground(bleCube, Color.green.getRGB());
				}
			}else if(str.equals("7")){
				//闪烁
				for (BleCube bleCube : connCubes) {
					xcaseManager.twinkleCube(bleCube, 3);
				}
			}else if(str.equals("8")){
				//发送字符串
				String[] imageTests = new String[connCubes.size()];
				for(int i=0; i<connCubes.size(); i++){
					String path = xcaseManager.createStringImageFile(i+"", Color.red.getRGB(), Color.white.getRGB(), "resource/");
					imageTests[i] = path;
				}
				if(connCubes != null){
					xcaseManager.displayCubesImages(xcaseManager.getCubes(), imageTests, new SendImageCallback() {
						
						@Override
						public void onSendImageSuccess() {
							// TODO Auto-generated method stub
							LOGGER.info("image send success");
						}
						
						@Override
						public void onSendImageFail(BleCube cube, int errorCode) {
							// TODO Auto-generated method stub
							LOGGER.info("image send failed");
						}
					});
				}
			}else if(str.equals("9")){
				//缓存图片测试
				String[] imageTests = new String[connCubes.size()];
				for(int i=0; i<connCubes.size(); i++){
					String path = xcaseManager.createStringImageFile(i+10+"", Color.GREEN.getRGB(), Color.gray.getRGB(), "resource/");
					imageTests[i] = path;
				}
				if(connCubes != null){
					xcaseManager.cacheCubesImages(xcaseManager.getCubes(), imageTests);
				}
			}else if(str.equals("10")){
				//直接显示缓存图片
				String[] imageTests = new String[connCubes.size()];
				for(int i=0; i<connCubes.size(); i++){
					String path = xcaseManager.createStringImageFile(i+10+"", Color.GREEN.getRGB(), Color.gray.getRGB(), "resource/");
					imageTests[i] = path;
				}
				if(connCubes != null){
					xcaseManager.displayCubesImages(xcaseManager.getCubes(), imageTests, new SendImageCallback() {
						
						@Override
						public void onSendImageSuccess() {
							// TODO Auto-generated method stub
							LOGGER.info("image send success");
						}
						
						@Override
						public void onSendImageFail(BleCube cube, int errorCode) {
							// TODO Auto-generated method stub
							LOGGER.info("image send failed");
						}
					});
				}
			}else{
				LOGGER.info("not support command");
			}
		}//end of while
	
	}

}
