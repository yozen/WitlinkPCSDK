package com.xcase.sdk.protocol;

public class XCaseCommon {

	public static final int HEADER = 0xFAFA;
	//协议数据类型
	public static final byte SCAN_TYPE = 0x01;
	public static final byte DIS_TYPE = 0x02;
	public static final byte CMD_TYPE = 0x03;
	public static final byte IMAGE_TYPE = 0x04;
	public static final byte NOTIFY_TYPE = 0x05;
	public static final byte LOST_TYPE = 0x06;
	public static final byte CHECK_TYPE = 0x07;
	public static final byte REGISTER_TYPE = 0x08;
	
	//开启扫描
	public static final byte SCAN_STRAT = 0x01;
	public static final byte SCAN_STOP = 0x00;
}
