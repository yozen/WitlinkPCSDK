package com.xcase.sdk.protocol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.util.DataUtil;

public class XCaseProtocolAdapter extends ProtocolAdapter{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(XCaseProtocolAdapter.class);
	
	@Override
	public boolean dealData() {
		// TODO dealData
		boolean enableRead = false;		//标志是否可以处理数据
		
		if(innerBuf.size()<=8){		//至少先收到8个字节再判断
			enableRead = false;
			return enableRead;
		}
		
		//首先搜索0xFAFA
		if(hasFound < 0){
			int i=0;
			for(i=0;i<innerBuf.size();i++){
				if(innerBuf.get(i) == (byte)0xFA && 
						(i != (innerBuf.size()-1))){
					//找到两个连续的0xFA
					if(innerBuf.get(i+1) == (byte)0xFA){
						hasFound = i;
//						LOGGER.debug("found header");
						break;
					}
				}
			}
			if(i >= innerBuf.size()-1 || hasFound < 0){
				//没有找到：(1)找到结尾还没找到;(2)标志位还是0
				enableRead = false;
				return enableRead;
			}
		}
		try{
			//首先判断报文头
			if(innerBuf.get(hasFound) == (byte)0xFA && innerBuf.get(1+hasFound) == (byte)0xFA){
				int bufLen = innerBuf.size() - hasFound;				//当前总报文长度
				if((hasFound+7) >= innerBuf.size()){
					enableRead = false;
				}else{
					dataFieldLen = (innerBuf.get(hasFound+7)&0xff)<<8 
									| innerBuf.get(hasFound+6)&0xff; 	//数据域长度
					if(dataFieldLen >= 0x100){
						//超长，则清除
						innerBuf.clear();
						hasFound = -1;
						enableRead = false;
					}
					int needNum = dataFieldLen + 8;
					if(bufLen < needNum){
						//缺数据
						enableRead = false;
					}else if(bufLen > needNum){
						//多数据
						readLen = needNum;
						enableRead = true;			
					}else{
						//相等数据
						readLen = needNum;
						enableRead = true;
					}
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return enableRead;
	}

	@Override
	public byte[] getCorrectByte() {
		// TODO getCorrectByte
		int limit = innerBuf.size();
		
		byte[] arrByte = get(hasFound, readLen);
		
		if(limit>readLen){
			//粘包情况
			byte[] remaingByte = get(readLen,limit-readLen);
			innerBuf.clear();
			put(remaingByte);
			LOGGER.debug("remaining：" + DataUtil.bytesToHexString(remaingByte, false, remaingByte.length));
		}else{
			innerBuf.clear();
		}
		
		hasFound = -1;
		
		return arrByte;
	}

	
	
}
