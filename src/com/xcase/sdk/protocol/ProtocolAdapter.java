package com.xcase.sdk.protocol;

import java.util.ArrayList;
import java.util.List;

public abstract class ProtocolAdapter {

	protected boolean saveNext;			//标志下次需等待新数据
	protected int dataFieldLen = 0;		//数据域长度
	protected int readLen = 0;			//表示要从缓存读出的数据长度
	protected final List<Byte> innerBuf;		//数据缓冲区
	protected int hasFound = -1;		//标识是否找到头，未找到为-1，找到为具体位置
	
	public ProtocolAdapter(){
		innerBuf = new ArrayList<Byte>();
	}
	
	/**
	 * 存
	 * @param data
	 */
	public final void put(byte[] data){
		for (byte b : data) {
			innerBuf.add(b);
		}
	}
	/**
	 * 取
	 * @param begin
	 * @param end
	 * @return
	 */
	public final byte[] get(int begin,int size){
		
		byte[] ret = new byte[size];
		for(int i=0;i<size;i++){
			ret[i] = innerBuf.get(i+begin);
		}
		return ret;
	}
	
	abstract public boolean dealData();
	
	abstract public byte[] getCorrectByte();
}
