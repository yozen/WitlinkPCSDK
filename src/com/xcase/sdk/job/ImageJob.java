package com.xcase.sdk.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.xcase.sdk.core.BleCube;


public class ImageJob implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO execute
		BleCube cube = (BleCube)context.getJobDetail().getJobDataMap().get(BleCube.CUBE_KEY);
		Object imageObj = context.getJobDetail().getJobDataMap().get(BleCube.IMAGE_KEY);
		int imageFlag = 0;
		if(imageObj != null){
			imageFlag = (int)imageObj;
		}
		byte[] imageData = null;
		if(imageFlag == BleCube.IMAGE_VALUE_NORMAL){
			//正常传图
			imageData = cube.getCurrentPieceImageContent();
		}else if(imageFlag == BleCube.IMAGE_VALUE_ERROR){
			//补发传图
			imageData = cube.getReTransImageContent();
		}
		if(imageData != null){
			cube.sendImageWithID(0, imageData);
		}else{
			//发送check命令
			cube.send_check();
		}
	}

}
