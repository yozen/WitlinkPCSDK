package com.xcase.sdk.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.xcase.sdk.core.BleCube;

public class CommandJob implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO execute
		BleCube cube = (BleCube)context.getJobDetail().getJobDataMap().get(BleCube.CUBE_KEY);
		Object commandObj = context.getJobDetail().getJobDataMap().get(BleCube.COMMMAND_KEY);
		int commandFlag = 0;
		if(commandObj != null){
			commandFlag = (int)commandObj;
		}
		switch(commandFlag){
		case BleCube.CONTENT_END:
			cube.send_content_end(0x00);
			break;
		case BleCube.CONTENT_CHECK:
			cube.send_check();
			break;
		}
	}

}
