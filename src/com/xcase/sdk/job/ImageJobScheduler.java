package com.xcase.sdk.job;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageJobScheduler {

	private final static Logger LOGGER = LoggerFactory.getLogger(ImageJobScheduler.class);
	
	private SchedulerFactory sf;
	private Scheduler sched;
	
	public ImageJobScheduler(){
		try{
			sf = new StdSchedulerFactory();
			sched = sf.getScheduler();
			sched.start();
			
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	public void run(JobDetail job, Trigger trigger){	
		try{
			sched.deleteJob(job.getKey());		
			sched.scheduleJob(job, trigger);
			
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	public void deleteJob(JobDetail job){
		try{
			sched.deleteJob(job.getKey());
		}catch (Exception e) {
			// TODO: handle exception
			LOGGER.debug(e.getMessage());
		}
	}
}
