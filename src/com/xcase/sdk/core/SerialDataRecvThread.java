package com.xcase.sdk.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.module.XCaseMsg;
import com.xcase.sdk.module.XCaseMsgBinCoder;
import com.xcase.sdk.module.XCaseMsgCoder;
import com.xcase.sdk.protocol.ProtocolAdapter;
import com.xcase.sdk.protocol.XCaseProtocolAdapter;
import com.xcase.sdk.util.DataUtil;

import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class SerialDataRecvThread extends Thread{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(SerialDataRecvThread.class);
	
	private Central centralThread;
	private SerialDataListener dataListener;
	private XCaseMsgCoder binCoder;
	private ProtocolAdapter adapter;
	
	private boolean isExit;
	
	public SerialDataRecvThread(Central centralThread, InputStream in, String name){
		this.centralThread = centralThread;
		binCoder = new XCaseMsgBinCoder();
		adapter = new XCaseProtocolAdapter();
		dataListener = new SerialDataListener(adapter, in);
		setName(name);
	}
	
	public SerialDataListener getSerialDataListener(){
		return dataListener;
	}
	
	public boolean isExit() {
		return isExit;
	}

	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}

	@Override
	public void run() {
		// TODO run
		LOGGER.info("Start SerialDataRecvThread, " + Thread.currentThread());
		while(true){
			if(isExit){
				break;
			}
			try{
				boolean ret = adapter.dealData();
				if(ret){
					byte[] byteArray = adapter.getCorrectByte();
					LOGGER.debug("received: " + DataUtil.bytesToHexString(byteArray, false, byteArray.length));
					XCaseMsg msg = binCoder.fromXCase(byteArray);
					centralThread.put(msg);
				}
				Thread.sleep(1);
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		LOGGER.info("Exit " + Thread.currentThread());
	}
	/**
	 * 串口数据监听内部类
	 * @author youxian
	 *
	 */
	protected class SerialDataListener implements SerialPortEventListener{
		
		private InputStream in;
        private List<Byte> buffer;
		private ProtocolAdapter adapter;
		
		public SerialDataListener(ProtocolAdapter adapter, InputStream in){
			this.adapter = adapter;
			this.in = in;
			this.buffer = new ArrayList<Byte>();
		}
		@Override
		public void serialEvent(SerialPortEvent event) {
			// TODO Auto-generated method stub
			switch(event.getEventType()){
			case SerialPortEvent.DATA_AVAILABLE:{
					int data;
					try{
		                while ( ( data = in.read()) > -1 )
		                {
		                	buffer.add((byte) data);
		                }
		                byte[] dataArray = DataUtil.arrayListToBytes(buffer);
		                this.buffer.clear();
		                LOGGER.info("recv data: " + DataUtil.bytesToHexString(dataArray, false, dataArray.length));
		                adapter.put(dataArray);
		                
		            }
		            catch ( IOException e )
		            {
		                e.printStackTrace();
		            }      
				}
				break;
				
			}
			
		}		
	}
	
	
	
	
	
}
