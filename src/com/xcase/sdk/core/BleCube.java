package com.xcase.sdk.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.arrangement.CubeGroup;
import com.xcase.sdk.arrangement.Point;
import com.xcase.sdk.job.CommandJob;
import com.xcase.sdk.job.ImageJob;
import com.xcase.sdk.job.ImageJobScheduler;
import com.xcase.sdk.listener.CentralEvent;
import com.xcase.sdk.listener.CentralEventListener;
import com.xcase.sdk.module.WLPImage;
import com.xcase.sdk.module.XCaseMsg;
import com.xcase.sdk.module.XCaseMsgCoder;
import com.xcase.sdk.payload.CommadPayload;
import com.xcase.sdk.payload.ImagePayload;
import com.xcase.sdk.payload.NotifyPayload;
import com.xcase.sdk.protocol.XCaseCommon;
import com.xcase.sdk.util.DataUtil;
import com.xcase.sdk.xinterface.XCaseProtocol;

/**
 * 该类代表一个小块
 * @author youxian
 *
 */
public class BleCube implements CentralEventListener{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(BleCube.class);
	
	public final static String CUBE_KEY = "cube";
	public final static String COMMMAND_KEY = "command";
	public final static String IMAGE_KEY = "image";				//发送图片数据的key，当value=1时表示正常发送，2表示补发
	public final static String CHECK_ACK_KEY = "checkAck";
	public final static String CONTENT_END_ACK_KEY = "contentEndAck";
	public final static int IMAGE_VALUE_NORMAL = 1;
	public final static int IMAGE_VALUE_ERROR = 2;
	
	private final static int SPEED = 10;
	private final static int TIMEOUT = 2000;
	private final static int TIMEOUTCNT = 3;
	private final static int CONTENT_START = 0xA602;
	public final static int CONTENT_CHECK = 0xA601;
	public final static int CONTENT_END = 0xA603;
	private final static int DRAW_FRAME = 0xA503;
	private final static int TWINKLE = 0xA504;
	private final static int BACKGROUND_COLOR = 0xA501;
	private final static int RESET = 0xA606;
	
	private final static byte[] QUERY_DEVICE_CONFIG = {(byte) 0xA6, 0x04, 0x01};
	private final static int MAX_PACKET_DATA_SIZE = 19;
	private final static int MAX_CUBE_BUFFER_SIZE = 3002+494;
	private final static int MAX_BYTE_PER_FRAGEMENT = MAX_CUBE_BUFFER_SIZE-4*MAX_PACKET_DATA_SIZE;		//每帧最大字节数
	private final static int MAX_PACKET_PER_FRAGEMENT = MAX_BYTE_PER_FRAGEMENT/MAX_PACKET_DATA_SIZE;	//每帧最大编号
	
	private int currentPiece = 0;			//编号从0开始
	private int currentFragement = 0;		//当前fragement，编号从0开始	
	private int lastSendedPiece = 0;		//上一次发送的总packet数
	private int connHandle = 0;				//连接handle编号,实际对应的蓝牙发送通道
	private int cubeID = 1;					//小块编号(用户排列组合识别，该编号为connHandle+1)
	private int centralID = 0;				//所对应的主机编号
	private Queue<Integer> reTranImage;		//补发图像数据队列
	private SerialDataSenderThread senderThread;
	private XCaseMsgCoder coder;
	private WLPImage wlpImage;
	private JobDetail imageJob;
	private JobDetail commandJob;
	private ImageJobScheduler jobScheduler;
	private Map<String, Boolean> ackMap;
	private int checkTimeoutCnt = 0;
	private int endTimeoutCnt = 0;
	// 小块属性
	@SuppressWarnings("unused")
	private int maxPicNum = 1;
	@SuppressWarnings("unused")
	private int maxPicLen = MAX_CUBE_BUFFER_SIZE;
	@SuppressWarnings("unused")
	private int shakeRepInterval = 0;
	private int lcdWidth = 240;
	private int lcdHeight = 240;
	private int sideState = 0;
	@SuppressWarnings("unused")
	private int currentPost = 0;
	@SuppressWarnings("unused")
	private int testModeOn = 0;
	
	//testing
	private long currentTimes = 0;
	//排列分组属性
	public int rotate;
    public CubeGroup group;
    public final Point point;
    public final List<Integer> changedRecord;
    public String address = "";
    //小块各种动作回调
    private XCaseProtocol.SendImageCallback mSendImageCallback;
    public XCaseProtocol.ActionCallback actionCallback;
	//缓存图片
    public String cacheImageName;
    public String contentName;
    private boolean isCacheImage;		//表示当前这轮发送的图片缓存在小块中，不显示
    //超时
    private ScheduledExecutorService scheduExec;
    
	public BleCube(int connHandle, SerialDataSenderThread senderThread,
			XCaseMsgCoder coder, byte[] deviceConfigRsp, int centralID){
		this.connHandle = connHandle;
		this.senderThread = senderThread;
		this.coder = coder;
		this.cubeID = centralID*10 + connHandle;
		this.centralID = centralID;
		jobScheduler = new ImageJobScheduler();
		imageJob = JobBuilder.newJob(ImageJob.class).withIdentity("image_job"+cubeID).build();
		commandJob = JobBuilder.newJob(CommandJob.class).withIdentity("command_job"+cubeID).build();
		reTranImage = new LinkedBlockingQueue<Integer>();
		ackMap = new HashMap<String, Boolean>();
		//小块信息
		if((deviceConfigRsp != null) && (deviceConfigRsp.length == 13)){
			maxPicNum = deviceConfigRsp[4];
			maxPicLen = deviceConfigRsp[5]&0xFF + ((deviceConfigRsp[6]&0xFF)<<8);
			shakeRepInterval = deviceConfigRsp[7];
			lcdWidth = deviceConfigRsp[8]&0xFF;
			lcdHeight = deviceConfigRsp[9]&0xFF;
			sideState = deviceConfigRsp[10]&0xFF;
			currentPost = deviceConfigRsp[11]&0xFF;
			testModeOn = deviceConfigRsp[12]&0xFF;
		}else{
			sendCmdWithID(QUERY_DEVICE_CONFIG);
		}
		
		point = new Point(0, 0);
        changedRecord = new CopyOnWriteArrayList<>();
        
        this.scheduExec = Executors.newScheduledThreadPool(3);
	}

	public int getCubeID(){
		return this.cubeID;
	}
	
	/**
	 * 给指定连接的小块发送命令
	 * @param index 外设编号
	 * @param id	小块连接id
	 * @param cmd	命令
	 */
	public void sendCmdWithID(byte[] cmd){
		try{
			CommadPayload commandPayload = new CommadPayload();
			commandPayload.setType(XCaseCommon.CMD_TYPE);
			commandPayload.setId((byte) connHandle);
			commandPayload.setCmdLen((byte) cmd.length);
			commandPayload.setCmd(cmd);
			XCaseMsg msg = XCaseMsg.getXCaseMsg(XCaseCommon.HEADER, 0, commandPayload);
			byte[] cmdArray = coder.toXCase(msg);
			senderThread.put(cmdArray);
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	/**
	 * 给指定连接的小块发送图片数据
	 * @param 指定外设编号
	 * @param id
	 * @param image
	 */
	public void sendImageWithID(int isColorTable, byte[] image){
		try{
			ImagePayload imagePayload = new ImagePayload();
			imagePayload.setType(XCaseCommon.IMAGE_TYPE);
			imagePayload.setId((byte) connHandle);
			imagePayload.setIsColorTable((byte) isColorTable);
			imagePayload.setImageLen((byte) image.length);
			imagePayload.setImages(image);
			XCaseMsg msg = XCaseMsg.getXCaseMsg(XCaseCommon.HEADER, 0, imagePayload);
			byte[] imageArray = coder.toXCase(msg);
			senderThread.put(imageArray);
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	/**
	 * 发送整张WLP图片
	 * @param id
	 * @param image
	 * @param listener
	 */
	public void sendWLPImage(byte[] image,String filename, boolean isCache, XCaseProtocol.SendImageCallback callback) throws Exception{
		currentTimes = System.currentTimeMillis();
		this.isCacheImage = isCache;
		this.mSendImageCallback = callback;
		
		if (filename.equals(cacheImageName)) {
            if (!isCache) {
                //直接发送end
            	startCommandTransfer(CONTENT_END);
            }
        } else {
        	this.currentPiece = 0;
			this.currentFragement = 0;
			this.lastSendedPiece = 0;
			this.wlpImage = makeWLPImage(image);
			//启动任务
			int xStart = (lcdWidth - wlpImage.getWidth()&0xFF)/2;
			int yStart = (lcdHeight - wlpImage.getHeight()&0xFF)/2;
			byte[] contentStart = makeContentStart(0x00, xStart, yStart, wlpImage.getWidth(), wlpImage.getHeight(), 
								0xFFFF, wlpImage.getCodecType(), wlpImage.getPacketNum(), wlpImage.getColorCount(), 
								wlpImage.getContentLen());
			sendCmdWithID(contentStart);
        }
	}
	/**
	 * 生成wlp对象
	 * @param imageData
	 * @return
	 */
	public WLPImage makeWLPImage(byte[] imageData){
		if(imageData == null)	return null;
		WLPImage wlpImage = new WLPImage();
		byte[] fileTypeBytes = new byte[3];
		System.arraycopy(imageData, 0, fileTypeBytes, 0, 3);
		String fileType = new String(fileTypeBytes);
		if(!fileType.equals(WLPImage.WLP)){
			return null;
		}
		wlpImage.setFileType(fileType);						//文件类型标识
		wlpImage.setHeaderLen(imageData[3]&0xFF);			//header长度
		int codecType = (imageData[4]&0xFF) + ((imageData[5]&0xFF)<<8);	//编码类型1--RLC 2--LZW
		wlpImage.setCodecType(codecType+1);					//编码类型
		wlpImage.setWidth(imageData[6]);					//图片宽度
		wlpImage.setHeight(imageData[7]);					//图片高度
		wlpImage.setColorCount(imageData[8]);				//color count
		int contentLen = (imageData[9]&0xFF) + ((imageData[10]&0xFF)<<8);
		wlpImage.setContentLen(contentLen);					//内容长度
		int backgroundColor = (imageData[11]&0xFF) + ((imageData[12]&0xFF)<<8);
		wlpImage.setBackgroundColor(backgroundColor);
		
		int temp = contentLen+(imageData[8]&0xFF)*2;
		byte[] content = new byte[temp];
		System.arraycopy(imageData, wlpImage.getHeaderLen(), content, 0, temp);
		wlpImage.setContent(content);						//内容
		
		int packetNum = 0;
		if(temp % MAX_PACKET_DATA_SIZE == 0){
			packetNum = temp/MAX_PACKET_DATA_SIZE;
		}else{
			packetNum = temp/MAX_PACKET_DATA_SIZE + 1;
		}
		wlpImage.setPacketNum(packetNum);					//packet总个数
		int fragementNum = 0;
		if(packetNum % MAX_PACKET_PER_FRAGEMENT == 0){
			fragementNum = packetNum/MAX_PACKET_PER_FRAGEMENT;
		}else{
			fragementNum = packetNum/MAX_PACKET_PER_FRAGEMENT + 1;
		}
		wlpImage.setFragementSize(fragementNum);			//fragement个数		
		return wlpImage;
	}
	
	private byte[] makeContentStart(int cubePicCtrlID, int xStart, int yStart, 
			int width, int height, int backgroudColor, int decodeType, int packetNum, 
			int RGB565ColorCount, int contentLen){
		byte[] content = new byte[15];
		content[0] = (byte) ((CONTENT_START>>8)&0xFF);
		content[1] = (byte) (CONTENT_START&0xFF);
		content[2] = (byte) cubePicCtrlID;
		content[3] = (byte) xStart;
		content[4] = (byte) yStart;
		content[5] = (byte) width;
		content[6] = (byte) height;
		content[7] = (byte) (backgroudColor&0xFF);
		content[8] = (byte) ((backgroudColor>>8)&0xFF);
		content[9] = (byte) decodeType;
		content[10] = (byte) (packetNum&0xFF);
		content[11] = (byte) ((packetNum>>8)&0xFF);
		content[12] = (byte) RGB565ColorCount;
		content[13] = (byte) (contentLen&0xFF);
		content[14] = (byte) ((contentLen>>8)&0xFF);
		
		return content;
	}
	
	private byte[] makeContentCheck(){
		byte[] check = new byte[2];
		check[0] = (byte) ((CONTENT_CHECK>>8)&0xFF);
		check[1] = (byte) (CONTENT_CHECK&0xFF);
		return check;
	}
	
	private byte[] makeContentEnd(int cubePicCtrlID, int fragementType, int lcdOnIferror){
		byte[] contentEnd = new byte[5];
		contentEnd[0] = (byte) ((CONTENT_END>>8)&0xFF);
		contentEnd[1] = (byte) (CONTENT_END&0xFF);
		contentEnd[2] = (byte) (cubePicCtrlID&0xFF);
		contentEnd[3] = (byte) (fragementType&0xFF);
		contentEnd[4] = (byte) (lcdOnIferror&0xFF);
		
		return contentEnd;
	}
	
	public byte[] makeDrawFrame(int rgb565, int framePix){
		byte[] drawFrame = new byte[5];
		drawFrame[0] = (byte) ((DRAW_FRAME>>8)&0xFF);
		drawFrame[1] = (byte) (DRAW_FRAME&0xFF);
		drawFrame[2] = (byte) ((rgb565>>8)&0xFF);
		drawFrame[3] = (byte) (rgb565&0xFF);
		drawFrame[4] = (byte) framePix;
		return drawFrame;
	}
	
	public byte[] makeTwinkle(int twinkleNum){
		byte[] twinkle = new byte[3];
		twinkle[0] = (byte) ((TWINKLE>>8)&0xFF);
		twinkle[1] = (byte) (TWINKLE&0xFF);
		twinkle[2] = (byte) twinkleNum;
		return twinkle;
	}
	
	public byte[] makeBackground(int rgb565){
		byte[] background = new byte[4];
		background[0] = (byte) ((BACKGROUND_COLOR>>8)&0xFF);
		background[1] = (byte) (BACKGROUND_COLOR&0xFF);
		background[2] = (byte) ((rgb565>>8)&0xFF);
		background[3] = (byte) (rgb565&0xFF);
		return background;
	}
	
	public byte[] makeReset(){
		byte[] reset = new byte[2];
		reset[0] = (byte) ((RESET>>8)&0xFF);
		reset[1] = (byte) (RESET&0xFF);
		return reset;
	}
	
	public void sendReset(){
		byte[] reset = makeReset();
		sendCmdWithID(reset);
	}
	
	public byte[] getCurrentPieceImageContent(){
		byte[] imageData = null;
		List<Byte> imageDataList = new ArrayList<Byte>();
		
		if(((currentPiece+currentFragement*MAX_PACKET_PER_FRAGEMENT) >= wlpImage.getPacketNum())
				|| (currentPiece >= MAX_PACKET_PER_FRAGEMENT)){
			LOGGER.info("in last packet, currentPiece=" + currentPiece + 
					" currentFragement=" + currentFragement + " packetNum=" + wlpImage.getPacketNum());
			lastSendedPiece = currentPiece;
			currentFragement++;
			currentPiece = 0;
			jobScheduler.deleteJob(imageJob);	//暂停传图任务
			return null;
		}else{
			for(int i=0; i<1; i++){
				//PC一次发送一条19字节
				imageData = getCurrentPieceImageContentByPiece(currentPiece, currentFragement);
				if(imageData != null){
					for(byte data : imageData){
						imageDataList.add(data);
					}
				}else{
					break;
				}
				currentPiece++;
			}
		}
		if(imageDataList.size() != 0){
			imageData = DataUtil.arrayListToBytes(imageDataList);
		}else{
			imageData = null;
		}

		return imageData;
	}
	
	public byte[] getCurrentPieceImageContentByPiece(int piece, int fragement){
		byte[] imageData = null;
		int imageDataLen = 0;
		if((piece + MAX_PACKET_PER_FRAGEMENT*fragement) < (wlpImage.getPacketNum()-1)){
			imageData = new byte[MAX_PACKET_DATA_SIZE+1];
			imageDataLen = MAX_PACKET_DATA_SIZE;
		}else if((piece + MAX_PACKET_PER_FRAGEMENT*fragement) == (wlpImage.getPacketNum()-1)){
			imageDataLen = wlpImage.getContent().length - (piece+MAX_PACKET_PER_FRAGEMENT*fragement)*MAX_PACKET_DATA_SIZE;
			imageData = new byte[imageDataLen+1];
		}else{
			//超过最大索引，返回空
			return null;
		}
		imageData[0] = (byte) piece;
		try{
			System.arraycopy(wlpImage.getContent(), piece*MAX_PACKET_DATA_SIZE + MAX_BYTE_PER_FRAGEMENT*fragement, imageData, 1, imageDataLen);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return imageData;
	}
	
	public byte[] getReTransImageContent(){
		byte[] imageData = null;
		if(!reTranImage.isEmpty()){
			int index = reTranImage.poll();
			imageData = getCurrentPieceImageContentByPiece(index, currentFragement-1);
		}else{
			jobScheduler.deleteJob(imageJob);
		}
		return imageData;
	}

	public void startTimeoutJob(final String ackType){
		scheduExec.schedule(new Runnable() {
            public void run() {
                if(!ackMap.get(ackType)){
                	LOGGER.info("CubeID: " + getCubeID() + "," + ackType + " command timeout"); 
                	timeoutEvent();
                }
            }
        }, TIMEOUT,TimeUnit.MILLISECONDS);
	}
	
	public void send_check(){
		LOGGER.info("CubeID: " + cubeID + ",send check");
		byte[] check = makeContentCheck();
		sendCmdWithID(check);
		ackMap.put(BleCube.CHECK_ACK_KEY, false);
		startTimeoutJob(BleCube.CHECK_ACK_KEY);
	}
	
	public void send_content_end(int picCtrID){
		if(!this.isCacheImage){
			LOGGER.info("CubeID: " + cubeID + ",send content end");
			byte[] contentEnd = makeContentEnd(picCtrID, 0x07, 0x01);	//生成content end指令
			sendCmdWithID(contentEnd);
			ackMap.put(BleCube.CONTENT_END_ACK_KEY, false);
			startTimeoutJob(BleCube.CONTENT_END_ACK_KEY);
		}
	}
	
	/**
	 * 发送命令超时处理函数
	 */
	public void timeoutEvent(){
		for (String ackType : ackMap.keySet()) {
			if(ackType.equals(CHECK_ACK_KEY)){
				boolean value = ackMap.get(ackType);
				if(!value){
					//check 超时
					LOGGER.info(getCubeID() + ",check command timeout, checkTimeoutCnt=" + checkTimeoutCnt);
					if(checkTimeoutCnt >= TIMEOUTCNT){
						//连续3次失败
						checkTimeoutCnt = 0;
						startCommandTransfer(CONTENT_END);
						LOGGER.info(getCubeID() + ",send check failed");
					}else{
						checkTimeoutCnt++;
						startCommandTransfer(CONTENT_CHECK);
					}
				}
			}else if(ackType.equals(CONTENT_END_ACK_KEY)){
				boolean value = ackMap.get(ackType);
				if(!value){
					//check 超时
					LOGGER.info(getCubeID() + ",content end timeout, endTimeoutCnt=" + endTimeoutCnt);
					if(endTimeoutCnt >= TIMEOUTCNT){
						//发送失败
						endTimeoutCnt = 0;
						LOGGER.info(getCubeID() +",send content end failed");
					}else{
						endTimeoutCnt++;
						startCommandTransfer(CONTENT_END);
					}
				}
			}
		}
	}
	
	private void startImageTransfer(int state){
		imageJob.getJobDataMap().put(CUBE_KEY, this);
		imageJob.getJobDataMap().put(BleCube.IMAGE_KEY, state);
		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("image_trigger"+cubeID)
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMilliseconds(SPEED).repeatForever())
				.build();
		jobScheduler.run(imageJob, trigger);
	}
	
	private void startCommandTransfer(int command){
		commandJob.getJobDataMap().put(CUBE_KEY, this);
		commandJob.getJobDataMap().put(BleCube.COMMMAND_KEY, command);
		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("command_trigger"+cubeID)
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withRepeatCount(0))
				.build();
		jobScheduler.run(commandJob, trigger);
	}
	
	@Override
	public void processEvent(CentralEvent event) {
		// TODO 反馈处理
		XCaseMsg msg = event.getMsg();
		if(msg.getPayload() instanceof NotifyPayload){
			NotifyPayload notifyPayload = (NotifyPayload)msg.getPayload();
			String command = DataUtil.bytesToHexString(notifyPayload.getCmd(), false, notifyPayload.getCmd().length);
			LOGGER.info("CubeID: " + getCubeID() + ", command ack: " + command);
			
			if(command.contains("B612")){
				//content start ack
				byte[] checkAck = notifyPayload.getCmd();
				if(checkAck[2] == 0x00){
					startImageTransfer(IMAGE_VALUE_NORMAL);
				}else if(checkAck[2] == 0x01){
					//长度超过小块内存
					
				}
			}else if(command.startsWith("B611")){
				//check ack
				LOGGER.info("CubeID: " + cubeID + " get B611 ack");
				checkTimeoutCnt = 0;
				ackMap.put(CHECK_ACK_KEY, true);
				byte[] checkAck = notifyPayload.getCmd();
				try{
					if(checkAck[2] == 0x00){
						if(currentFragement < (this.wlpImage.getFragementSize())){
							//开始传输下一帧图片数据
							startImageTransfer(IMAGE_VALUE_NORMAL);
						}else{
							//最后一帧,check成功，图片传输完成，发送content end
							if(this.isCacheImage){
								//缓存图片的时候，直接发送成功
								if (mSendImageCallback != null) {
					                mSendImageCallback.onSendImageSuccess();
					            }
							}else{
								//否则发送content end命令
								startCommandTransfer(CONTENT_END);
							}
						}
					}else if(checkAck[2] == 0x02){
						//错误
						if(this.isCacheImage){
							//缓存图片的时候，直接发送成功
							if (mSendImageCallback != null) {
				                mSendImageCallback.onSendImageFail(this, checkAck[2]);
				            }
						}else{
							startCommandTransfer(CONTENT_END);
						}
					}else if(checkAck[2] == 0x03){
						//图片传输缺失
						int framNum = checkAck[3];
						int framIndex = checkAck[4];
						for(int i=0; i<120; i++){
							int isSet = ((checkAck[5+(i>>3)]&0xFF)>>(i&0x07))&0x01;
							if(isSet != 1){
								if((i+(framIndex-1)*120) < lastSendedPiece){
									reTranImage.add(i+(framIndex-1)*120);
								}
							}
						}
						if(lastSendedPiece <= 0){
							LOGGER.info("CubeID: " + cubeID + ",last piece = 0, but still trigger retransfer");
							startCommandTransfer(CONTENT_END);
						}else{
							if(framIndex == framNum){
								//ack收全，可以开始重传
								startImageTransfer(IMAGE_VALUE_ERROR);
							}
						}
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}else if(command.startsWith("B613")){
				//content end ack
				endTimeoutCnt = 0;
				ackMap.put(CONTENT_END_ACK_KEY, true);
				byte[] contentEnd = notifyPayload.getCmd();
				if(contentEnd[2] == 0x00){
					//content end成功
					jobScheduler.deleteJob(imageJob);
					if(!this.isCacheImage){
						if (mSendImageCallback != null) {
			                mSendImageCallback.onSendImageSuccess();
			            }
					}
					LOGGER.info("耗时: " + (System.currentTimeMillis() - currentTimes));
				}else{
					if(!this.isCacheImage){
						if (mSendImageCallback != null) {
			                mSendImageCallback.onSendImageFail(this, contentEnd[2]);
			            }
					}
				}
			}else if(command.startsWith("B614")){
				byte[] deviceConfigRsp = notifyPayload.getCmd();
				if(deviceConfigRsp[2] == 0x01){
					//query
					maxPicNum = deviceConfigRsp[4];
					maxPicLen = deviceConfigRsp[5]&0xFF + ((deviceConfigRsp[6]&0xFF)<<8);
					shakeRepInterval = deviceConfigRsp[7];
					lcdWidth = deviceConfigRsp[8]&0xFF;
					lcdHeight = deviceConfigRsp[9]&0xFF;
					sideState = deviceConfigRsp[10]&0xFF;
					currentPost = deviceConfigRsp[11]&0xFF;
					testModeOn = deviceConfigRsp[12]&0xFF;
				}
			}else if(command.startsWith("CAC7")){
				//翻转
				byte[] turnRsp = notifyPayload.getCmd();
				int lastPost = turnRsp[2];
				int currentPost = turnRsp[3];
				if (null != actionCallback){
                    actionCallback.onTurn(BleCube.this, lastPost, currentPost);
                }
			}else if(command.startsWith("CAC6")){
				//摇晃
				byte[] shakeRsp = notifyPayload.getCmd();
				int shakeNum = (shakeRsp[2]&0xFF) | ((shakeRsp[3]&0xFF)<<8);
				if (null != actionCallback){
                    actionCallback.onShake(BleCube.this, shakeNum);
                }
			}else if(command.startsWith("CAC5")){
				//碰撞
				byte[] sideRsp = notifyPayload.getCmd();
                int side = sideRsp[2]&0xff;
                Integer cSide = Integer.valueOf(side - sideState);
                if (cSide != 0) {
                    if (changedRecord.contains(cSide)) {
                        changedRecord.remove(cSide);
                    }
                    changedRecord.add(0, cSide);
                    CubeGroup.handleCubeSideStateChanged(BleCube.this, cSide);
                }
                sideState = side;
			}
			
			
			
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO equals
		boolean ret = false;
		if(obj instanceof BleCube){
			BleCube cube = (BleCube)obj;
			if(cube.getCubeID() == this.cubeID){
				ret = true;
			}
		}
		return ret;
	}

}
