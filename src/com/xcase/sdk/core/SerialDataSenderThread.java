package com.xcase.sdk.core;

import java.io.OutputStream;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.util.DataUtil;

public class SerialDataSenderThread extends Thread{

	private final static Logger LOGGER = LoggerFactory.getLogger(SerialDataSenderThread.class);
	
	private Queue<byte[]> senderQueue;
	private OutputStream out;
	private boolean isExit;
	
	public SerialDataSenderThread(OutputStream out, String name){
		this.out = out;
		senderQueue = new LinkedBlockingQueue<byte[]>();
		setName(name);
	}

	public boolean put(byte[] ele){
		return senderQueue.add(ele);
	}
	
	public boolean isExit() {
		return isExit;
	}

	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}

	@Override
	public void run() {
		// TODO run
		LOGGER.info("Start SerialDataSenderThread, " + Thread.currentThread());
		while(true){
			if(isExit){
				break;
			}
			try {
				int sleepTime = 0;
				if(!senderQueue.isEmpty()){
					byte[] ele = senderQueue.poll();
					sleepTime = ele.length*1000/14400;
					LOGGER.info("sending: " + DataUtil.bytesToHexString(ele, false, ele.length));
					out.write(ele);
				}
				Thread.sleep(sleepTime);
			} catch (Exception e) {
				LOGGER.debug(e.getMessage());
			}
		}
		LOGGER.info("Exit " + Thread.currentThread());
	}
	
	
}
