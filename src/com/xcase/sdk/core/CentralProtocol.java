package com.xcase.sdk.core;

import java.util.List;

public class CentralProtocol {

	
	public interface CentralCallback{
		
		public void onScanResult(int centralID, List<BleCube> cubes);
	}
}
