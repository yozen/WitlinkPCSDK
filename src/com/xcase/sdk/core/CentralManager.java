package com.xcase.sdk.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.module.XCaseMsg;
import com.xcase.sdk.module.XCaseMsgBinCoder;
import com.xcase.sdk.module.XCaseMsgCoder;
import com.xcase.sdk.protocol.XCaseCommon;
import com.xcase.sdk.util.XCaseDelayer;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class CentralManager{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(CentralManager.class);
	
	private List<Central> centralList;
	private XCaseMsgCoder coder;					//协议编解码类	
	
	private static CentralManager xcaseManager;
	
	private CentralManager() throws IOException{
		coder = new XCaseMsgBinCoder();
		centralList = new ArrayList<Central>();
	}
	
	public static CentralManager getXCaseManager() throws IOException{
		synchronized (CentralManager.class) {
			if(xcaseManager == null){
				xcaseManager = new CentralManager();
			}
		}
		
		return xcaseManager;
	}
	
	public String getPortTypeName ( int portType ){
        switch ( portType ){
            case CommPortIdentifier.PORT_I2C:
                return "I2C";
            case CommPortIdentifier.PORT_PARALLEL:
                return "Parallel";
            case CommPortIdentifier.PORT_RAW:
                return "Raw";
            case CommPortIdentifier.PORT_RS485:
                return "RS485";
            case CommPortIdentifier.PORT_SERIAL:
                return "Serial";
            default:
                return "unknown type";
        }
    }
	/**
	 * 初始化串口设备，检索共有几个USB CENTRAL外设
	 * @return 
	 */
	public List<Central> initSerial(){
		
		try{
			Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
			int i = 0;
			while(portEnum.hasMoreElements()){
				CommPortIdentifier portIdentifier = (CommPortIdentifier)portEnum.nextElement();
				if(portIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL){
					i++;	//主机编号从1开始
					String portName = portIdentifier.getName();
					CommPort commPort = portIdentifier.open(portName,2000);
					SerialPort serialPort = (SerialPort) commPort;
	                serialPort.setSerialPortParams(115200,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
	                serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN);
	                InputStream in = serialPort.getInputStream();
	                OutputStream out = serialPort.getOutputStream();
	                Central central = new Central();
					SerialDataRecvThread recvThread = new SerialDataRecvThread(central, in, "Central-"+i+"thread");
					SerialDataSenderThread senderThead = new SerialDataSenderThread(out, "Central-"+i+"thread");
					central.setRecvThread(recvThread);
					central.setSenderThread(senderThead);
	                serialPort.addEventListener(recvThread.getSerialDataListener());
	                serialPort.notifyOnDataAvailable(true);
					//发送注册信息
	                byte[] payload = { 0x08, (byte) i};
					XCaseMsg msg = XCaseMsg.getXCaseMsg(XCaseCommon.HEADER, 0, payload.length, payload);
					byte[] registerCmd = coder.toXCase(msg);
					recvThread.start();
					senderThead.start();
					central.start();
					//将命令加入发送线程的发送队列
					senderThead.put(registerCmd);
					//等待是否收到反馈
					while(XCaseDelayer.startTimer(500, 100) != 1){
						if(central.getValidCentralFlag()){
							//正确
							central.setCentralID(i);
							centralList.add(central);
							break;
						}
					}
					if(!central.getValidCentralFlag()){
						recvThread.setExit(true);
						senderThead.setExit(true);
						central.setExit(true);
						
						serialPort.removeEventListener();
						serialPort.close();
						commPort.close();
					}
					XCaseDelayer.closeTimer();
					
				}
				XCaseDelayer.closeTimer();
			}
		}catch (Exception e) {
			XCaseDelayer.closeTimer();
			LOGGER.debug(e.getMessage());
		}
		LOGGER.info("found " + centralList.size() + " central");
		return centralList;
	}
	
	
	
	
	
	
	
	
	
	
	

	
	
}
