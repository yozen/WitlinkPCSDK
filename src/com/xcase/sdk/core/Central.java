package com.xcase.sdk.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.arrangement.CubeGroup;
import com.xcase.sdk.core.CentralProtocol.CentralCallback;
import com.xcase.sdk.listener.CentralEvent;
import com.xcase.sdk.listener.CentralEventListener;
import com.xcase.sdk.module.XCaseMsg;
import com.xcase.sdk.module.XCaseMsgBinCoder;
import com.xcase.sdk.module.XCaseMsgCoder;
import com.xcase.sdk.payload.CheckAckPayload;
import com.xcase.sdk.payload.DisconnPayload;
import com.xcase.sdk.payload.NotifyPayload;
import com.xcase.sdk.payload.RegisterInfoPayload;
import com.xcase.sdk.payload.ScanAckPayload;
import com.xcase.sdk.payload.ScanPayload;
import com.xcase.sdk.protocol.XCaseCommon;
import com.xcase.sdk.util.DataUtil;
import com.xcase.sdk.xinterface.XCaseProtocol;
import com.xcase.sdk.xinterface.XCaseProtocol.ConnectCallback;


/**
 * 该类表示一个USB外设
 * @author youxian
 *
 */
public class Central extends Thread implements CentralEventListener{

	private final static Logger LOGGER = LoggerFactory.getLogger(Central.class);
	
	public final static int ALL = 0xFFFF;
	
	private Map<Integer, BleCube> cubeMap;
	private Map<Integer, CentralEventListener> cubeListenerMap;
	private SerialDataRecvThread recvThread;		//接收线程
	private SerialDataSenderThread senderThread;	//发送线程
	private List<CentralEventListener> listeners;	//监听器列表
	private CentralEventListener xcaseEventListener;//监听
	private Queue<XCaseMsg> msgQueue;				//反馈信息队列
	private XCaseMsgCoder coder;					//协议编解码类
	private Map<Integer, byte[]> initCubesInfo;		//设备初始信息
	private int centralID;							//外设唯一标识
	private boolean isValidCentral;					//是否有效的USB外设
	private boolean isExit;
	
	private XCaseProtocol.ConnectCallback mConnCallback;
	private CentralProtocol.CentralCallback mCentralCallback;
	
	private int wantedCubeNum;
	
	public Central(){
		this.xcaseEventListener = this;
		this.isValidCentral = false;
		listeners = new ArrayList<CentralEventListener>();
		cubeListenerMap = new HashMap<Integer, CentralEventListener>();
		coder = new XCaseMsgBinCoder();
		msgQueue = new LinkedBlockingQueue<XCaseMsg>();
		cubeMap = new HashMap<Integer, BleCube>();
		initCubesInfo = new HashMap<Integer, byte[]>();
	}
	
	public SerialDataRecvThread getRecvThread() {
		return recvThread;
	}

	public void setRecvThread(SerialDataRecvThread recvThread) {
		this.recvThread = recvThread;
	}

	public SerialDataSenderThread getSenderThread() {
		return senderThread;
	}

	public void setSenderThread(SerialDataSenderThread senderThread) {
		this.senderThread = senderThread;
	}

	public int getCentralID() {
		return centralID;
	}

	public void setCentralID(int centralID) {
		this.centralID = centralID;
	}

	public Map<Integer, BleCube> getCubes(){
		return this.cubeMap;
	}
	
	public int getWantedCubeNum() {
		return wantedCubeNum;
	}

	public void setWantedCubeNum(int wantedCubeNum) {
		this.wantedCubeNum = wantedCubeNum;
	}

	public boolean getValidCentralFlag(){
		return isValidCentral;
	}
	
	public boolean isExit() {
		return isExit;
	}

	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}

	/**
	 * 加入反馈对象
	 * @param msg
	 * @return
	 */
	public boolean put(XCaseMsg msg){
		return msgQueue.add(msg);
	}
	/**
	 * 发起连接
	 * @param maxConnNum 指定最大连接数
	 * @param timeout 指定扫描超时时间
	 */
	public void connectWithIDs(int maxConnNum, long timeout, ConnectCallback connCallback, CentralCallback centralCallback){
		try{
			this.mConnCallback = connCallback;
			this.mCentralCallback = centralCallback;
			
			ScanPayload scanPayload = new ScanPayload();
			scanPayload.setType(XCaseCommon.SCAN_TYPE);
			scanPayload.setScanOn(XCaseCommon.SCAN_STRAT);
			if(maxConnNum > 8){
				scanPayload.setMaxConn((byte) 8);
			}else{
				scanPayload.setMaxConn((byte) maxConnNum);
			}
			scanPayload.setTimeout(timeout);
			XCaseMsg msg = XCaseMsg.getXCaseMsg(XCaseCommon.HEADER, 0, scanPayload);
			byte[] scanCmd = coder.toXCase(msg);
			senderThread.put(scanCmd);
			
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	/**
	 * 断开指定连接或全部断开
	 * @param IDs——小块的连接编号，
	 */
	public void disconnectWithIDs(int IDs){
		try{
			DisconnPayload disPayload = new DisconnPayload();
			disPayload.setType(XCaseCommon.DIS_TYPE);
			if(IDs == 0xFFFF){
				cubeMap.clear();
				cubeListenerMap.clear();
				disPayload.setDisNum((byte)0xFF);
				disPayload.setDisIDs(0xFFFF);
			}else{
				disPayload.setDisIDs(IDs);
				disPayload.setDisNum(1);
			}
			XCaseMsg msg = XCaseMsg.getXCaseMsg(XCaseCommon.HEADER, 0, disPayload);
			byte[] disCmd = coder.toXCase(msg);
			senderThread.put(disCmd);
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
		//发送reset
//		if(IDs == 0xFFFF){
//			for(Integer cubeConn : cubes.keySet()){
//				BleCube cube = cubes.get(cubeConn);
//				cube.sendReset();
//			}
//			cubes.clear();
//			cubeListeners.clear();
//		}else{
//			
//		}
	}
	/**
	 * 查询主机
	 * @param index		指定USB外设
	 * @param listener	指定监听器
	 */
	public void checkCentral(CentralEventListener listener){
		try{
			byte[] payload = DataUtil.hexStringToBytes("07");
			XCaseMsg msg = XCaseMsg.getXCaseMsg(XCaseCommon.HEADER, 0, payload.length, payload);
			byte[] checkCmd = coder.toXCase(msg);
			senderThread.put(checkCmd);
			if(listener != null){
				listeners.add(listener);
			}
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	/**
	 * 停止扫描
	 * @param index 指定外设编号
	 */
	public void stopScan(){
		try{
			ScanPayload scanPayload = new ScanPayload();
			scanPayload.setType(XCaseCommon.SCAN_TYPE);
			scanPayload.setScanOn(XCaseCommon.SCAN_STOP);
			scanPayload.setMaxConn((byte) 0);
			scanPayload.setTimeout(0);
			XCaseMsg msg = XCaseMsg.getXCaseMsg(XCaseCommon.HEADER, 0, scanPayload);
			byte[] scanCmd = coder.toXCase(msg);
			senderThread.put(scanCmd);
		}catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	private void notifyXCaseEvent(XCaseMsg msg){
		CentralEvent event = new CentralEvent(this);
		event.setMsg(msg);
		xcaseEventListener.processEvent(event);
		if(msg.getPayload() instanceof NotifyPayload){
			NotifyPayload notifyPayload = (NotifyPayload)msg.getPayload();
			CentralEventListener listener = cubeListenerMap.get(notifyPayload.getId());
			if(listener != null){
				listener.processEvent(event);
			}
		}
	}

	@Override
	public void run() {
		LOGGER.info("Start CentralThread");
		while(true){
			if(isExit){
				break;
			}
			try {
				if(msgQueue != null){
					if(!msgQueue.isEmpty()){
						XCaseMsg msg = msgQueue.poll();
						notifyXCaseEvent(msg);
					}
				}
				Thread.sleep(5);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		LOGGER.info("Exit " + Thread.currentThread());
	}

	

	@Override
	public void processEvent(CentralEvent event) {
		// TODO 反馈处理
		XCaseMsg msg = event.getMsg();
		if(msg.getPayload() instanceof RegisterInfoPayload){
			//注册包反馈
			isValidCentral = true;
			LOGGER.info("Version: " + ((RegisterInfoPayload)(msg.getPayload())).getVersion());
		}else if(msg.getPayload() instanceof CheckAckPayload){
			//检查包反馈
			CheckAckPayload checkAckPayload = ((CheckAckPayload)msg.getPayload());
			int IDs = checkAckPayload.getConnIDs();
			int connNum = checkAckPayload.getConnNum();
			String cubeIDStr = "";
			if(connNum > 0){
				int id = 0;
				for(int i=0;i<16;i++){
					id = (IDs>>i)&0x01;
					if(id == 0x01){
						cubeIDStr += (i+" ");
					}
				}
			}
			LOGGER.info("Version: " + checkAckPayload.getVersion() + 
					", Cube IDs: " + cubeIDStr + " Cube Num: " + connNum);
		}else if(msg.getPayload() instanceof ScanAckPayload){
			//扫描结果包反馈
			ScanAckPayload scanAckPayload = (ScanAckPayload)msg.getPayload();
			int IDs = scanAckPayload.getConnIDs();
			int connNum = scanAckPayload.getConnNum();
			String cubeIDStr = "";
			List<BleCube> bleCubes = new ArrayList<BleCube>();
			if(connNum > 0){
				int flag = 0;
				for(int i=0;i<16;i++){
					flag = (IDs>>i)&0x01;
					if(flag == 0x01){
						if(!cubeMap.containsKey(i)){
							//实例化小块对象
							byte[] deviceConfigInit = initCubesInfo.get(i);
							BleCube cube = new BleCube(i, senderThread, coder, 
									deviceConfigInit, getCentralID());
							cube.group = new CubeGroup(cube);
							cubeMap.put(i, cube);
							cubeListenerMap.put(i, cube);
							bleCubes.add(cube);
						}
						cubeIDStr += (i+" ");
					}
				}
			}
			this.mConnCallback.onConnect(bleCubes);
			this.mCentralCallback.onScanResult(this.centralID, bleCubes);
			LOGGER.info("CentralID: " + scanAckPayload.getDeviceID() + 
						", Cube IDs: " + cubeIDStr + ", Cube Num: " + connNum);
		}else if(msg.getPayload() instanceof NotifyPayload){
			//首次连接时的数据
			NotifyPayload notifyPayload = (NotifyPayload)msg.getPayload();
			String command = DataUtil.bytesToHexString(notifyPayload.getCmd(), false, notifyPayload.getCmd().length);
			if(command.startsWith("B61403")){
				int connHandle = notifyPayload.getId();
				byte[] deviceConfigInit = notifyPayload.getCmd();
				initCubesInfo.put(connHandle, deviceConfigInit);
			}
		}else if(msg.getPayload() instanceof DisconnPayload){
			//上报小块断开连接
			int disIds =  (((DisconnPayload)msg.getPayload()).getDisIDs());
			int flag = 0;
			for(int i=0;i<16;i++){
				flag = (disIds>>i)&0x01;
				if(flag == 0x01){
					BleCube bc = cubeMap.get(i);
					if(bc != null){
						mConnCallback.onDisconnect(bc);
						cubeMap.remove(i);
						cubeListenerMap.remove(i);
						LOGGER.info(getCentralID() + ", Remain Connected Cube Size: " + cubeMap.size() + 
								", Disconnected ID: " + i);
					}
				}
			}
		}
		
		
	}
	
	
	
	
}
