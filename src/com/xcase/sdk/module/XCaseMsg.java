package com.xcase.sdk.module;

import com.xcase.sdk.payload.BasicPayload;
import com.xcase.sdk.payload.CommadPayload;
import com.xcase.sdk.payload.DisconnPayload;
import com.xcase.sdk.payload.ImagePayload;
import com.xcase.sdk.payload.ScanPayload;
import com.xcase.sdk.protocol.XCaseCommon;
import com.xcase.sdk.util.CRC16Util;
import com.xcase.sdk.util.DataUtil;

public class XCaseMsg {

	private int header;
	private int crc;
	private int seq;
	private int length;
	private byte[] payloadArray;
	private BasicPayload payload;
	
	public XCaseMsg(int header, int crc, int seq, int length, byte[] payload){
		this.header = header;
		this.crc = crc;
		this.seq = seq;
		this.length = length;
		this.payloadArray = payload;
	}
	
	public XCaseMsg(int header, int crc, int seq, int length, byte[] payloadArray, BasicPayload payload){
		this.header = header;
		this.crc = crc;
		this.seq = seq;
		this.length = length;
		this.payloadArray = payloadArray;
		this.payload = payload;
	}
	
	public int getHeader() {
		return header;
	}
	public void setHeader(int header) {
		this.header = header;
	}
	public int getCrc() {
		return crc;
	}
	public void setCrc(int crc) {
		this.crc = crc;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public byte[] getPayloadArray() {
		return payloadArray;
	}
	public void setPayloadArray(byte[] payload) {
		this.payloadArray = payload;
	}
	public BasicPayload getPayload() {
		return payload;
	}
	public void setPayload(BasicPayload payload) {
		this.payload = payload;
	}

	public static XCaseMsg getXCaseMsg(int header, int seq, int length, byte[] payload){
		byte[] crcData = new byte[4+length];
		crcData[0] = (byte) (seq&0xff);
		crcData[1] = (byte) ((seq>>8)&0xff);
		crcData[2] = (byte) (length&0xff);
		crcData[3] = (byte) ((length>>8)&0xff);
		System.arraycopy(payload, 0, crcData, 4, length);
		int crc =  CRC16Util.check(crcData, 0xFFFF);
		
		return new XCaseMsg(header, crc, seq, length, payload);
	}
	
	public static XCaseMsg getXCaseMsg(int header, int seq, BasicPayload payload){
		byte[] payloadArray = null;
		XCaseMsg msg = null;
		int length = 0;
		switch(payload.getType()){
		case XCaseCommon.SCAN_TYPE:
			ScanPayload scanPayload = (ScanPayload)payload;
			payloadArray = new byte[5];
			length = 5;
			payloadArray[0] = XCaseCommon.SCAN_TYPE;
			payloadArray[1] = scanPayload.getScanOn();
			payloadArray[2] = scanPayload.getMaxConn();
			payloadArray[3] = (byte) (scanPayload.getTimeout()&0xFF);
			payloadArray[4] = (byte) ((scanPayload.getTimeout()>>8)&0xFF);
		break;
		case XCaseCommon.DIS_TYPE:
			DisconnPayload disPayload = (DisconnPayload)payload;
			payloadArray = new byte[4];
			length = 4;
			payloadArray[0] = XCaseCommon.DIS_TYPE;
			payloadArray[1] = (byte)disPayload.getDisNum();
			payloadArray[2] = (byte) (disPayload.getDisIDs()&0xFF);
			payloadArray[3] = (byte) ((disPayload.getDisIDs()>>8)&0xFF);
		break;
		case XCaseCommon.CMD_TYPE:
			CommadPayload commandPayload = (CommadPayload)payload;
			length = 2+commandPayload.getCmdLen();
			payloadArray = new byte[length];
			payloadArray[0] = XCaseCommon.CMD_TYPE;
			payloadArray[1] = commandPayload.getId();
			System.arraycopy(commandPayload.getCmd(), 0, payloadArray, 2, commandPayload.getCmdLen());
		break;
		case XCaseCommon.IMAGE_TYPE:
			ImagePayload imagePayload = (ImagePayload)payload;
			length = 3+imagePayload.getImageLen();
			payloadArray = new byte[length];
			payloadArray[0] = XCaseCommon.IMAGE_TYPE;
			payloadArray[1] = imagePayload.getId();
			payloadArray[2] = imagePayload.getIsColorTable();
			System.arraycopy(imagePayload.getImages(), 0, payloadArray, 3, imagePayload.getImageLen());
		break;
		}
		msg = getXCaseMsg(header, seq, length, payloadArray);
		
		return msg;
	}
	
	public String toString(){
		return "header=" + Integer.toHexString(header) + 
				" crc=" + Integer.toHexString(crc) + 
				" seq=" + Integer.toHexString(seq) + 
				" length=" + Integer.toHexString(length) + 
				" payload=" + DataUtil.bytesToHexString(payloadArray, false, payloadArray.length) +  
				"\r\n";
	}
}
