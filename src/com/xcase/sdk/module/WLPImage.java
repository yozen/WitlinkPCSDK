package com.xcase.sdk.module;

public class WLPImage {
	
	public static final String WLP = "WLP";
	
	private String fileType;		//文件类型标识
	private int headerLen;			//头长度
	private int codecType;			//编码类型
	private int width;
	private int height;
	private int colorCount;
	private byte[] content;
	private int packetNum;
	private int contentLen;
	
	private int fragementSize;		//总共多少帧，每帧最大3K
	private int backgroundColor;	//背景色
	
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public int getHeaderLen() {
		return headerLen;
	}
	public void setHeaderLen(int headerLen) {
		this.headerLen = headerLen;
	}
	public int getCodecType() {
		return codecType;
	}
	public void setCodecType(int codecType) {
		this.codecType = codecType;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getColorCount() {
		return colorCount;
	}
	public void setColorCount(int colorCount) {
		this.colorCount = colorCount;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public int getPacketNum() {
		return packetNum;
	}
	public void setPacketNum(int packetNum) {
		this.packetNum = packetNum;
	}
	public int getContentLen() {
		return contentLen;
	}
	public void setContentLen(int contentLen) {
		this.contentLen = contentLen;
	}
	public int getFragementSize() {
		return fragementSize;
	}
	public void setFragementSize(int fragementSize) {
		this.fragementSize = fragementSize;
	}
	public int getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
	
}
