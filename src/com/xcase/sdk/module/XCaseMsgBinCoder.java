package com.xcase.sdk.module;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.xcase.sdk.payload.BasicPayload;
import com.xcase.sdk.payload.CheckAckPayload;
import com.xcase.sdk.payload.DisconnPayload;
import com.xcase.sdk.payload.NotifyPayload;
import com.xcase.sdk.payload.RegisterInfoPayload;
import com.xcase.sdk.payload.ScanAckPayload;
import com.xcase.sdk.protocol.XCaseCommon;

public class XCaseMsgBinCoder implements XCaseMsgCoder{

	public static final int MIN_XCASE_LEN = 8;
	public static final int HEADER = 0xFAFA;
	public static final int CRC_MASK = 0xFFFF;
	
	
	@Override
	public byte[] toXCase(XCaseMsg msg) throws IOException {
		// TODO XCase对象转化为字节数组
		byte[] xcaseByteArray = new byte[8+msg.getLength()];
		xcaseByteArray[0] = (byte) (msg.getHeader()&0xFF);
		xcaseByteArray[1] = (byte) ((msg.getHeader()>>8)&0xFF);
		xcaseByteArray[2] = (byte) (msg.getCrc()&0xFF);
		xcaseByteArray[3] = (byte)((msg.getCrc()>>8)&0xFF);
		xcaseByteArray[4] = (byte) (msg.getSeq()&0xFF);
		xcaseByteArray[5] = (byte)((msg.getSeq()>>8)&0xFF);
		xcaseByteArray[6] = (byte) (msg.getLength()&0xFF);
		xcaseByteArray[7] = (byte)((msg.getLength()>>8)&0xFF);
		System.arraycopy(msg.getPayloadArray(), 0, xcaseByteArray, 8, msg.getLength());
		
		return xcaseByteArray;
	}

	@Override
	public XCaseMsg fromXCase(byte[] input) throws IOException {
		// TODO 字节数组转化为XCase对象
		if(input.length < MIN_XCASE_LEN){
			throw new IOException("ERROR LENGTH");
		}
		ByteArrayInputStream bs = new ByteArrayInputStream(input);
		DataInputStream in = new DataInputStream(bs);
		int header = in.readShort()&HEADER;
		if(header != HEADER){
			throw new IOException("BAD HEADER");
		}
		int crc = in.readShort()&CRC_MASK;
		int seq = in.readByte() + in.readByte()*256;
		int length = in.readByte() + in.readByte()*256;
		byte[] payloadArray = new byte[length];
		BasicPayload payload = null;
		int readSize = in.read(payloadArray);
		if(readSize != length){
			throw new IOException("PAYLOAD LENGTH EXCEPTION");
		}else{
			
			switch(payloadArray[0]){
			case (byte) (XCaseCommon.SCAN_TYPE|0x80):
				payload = new ScanAckPayload();
				payload.setType(payloadArray[0]);
				((ScanAckPayload)payload).setConnNum(payloadArray[1]);
				((ScanAckPayload)payload).setConnIDs(payloadArray[2]&0xFF + (payloadArray[3]&0xff)*256);
				((ScanAckPayload)payload).setDeviceID(payloadArray[4]);
				break;
			case (byte) (XCaseCommon.DIS_TYPE|0x80):
				payload = new DisconnPayload();
				payload.setType(payloadArray[0]);
				int ids = payloadArray[1]&0xFF + ((payloadArray[2]&0xFF)<<8);
				((DisconnPayload)payload).setDisIDs(ids);
				break;
			case (byte) (XCaseCommon.CHECK_TYPE|0x80):
				payload = new CheckAckPayload();
				payload.setType(payloadArray[0]);
				((CheckAckPayload)payload).setConnNum(payloadArray[1]);
				((CheckAckPayload)payload).setConnIDs(payloadArray[2]&0xFF + (payloadArray[3]&0xff)*256);
				byte[] versionArray = new byte[10];
				System.arraycopy(payloadArray, 4, versionArray, 0, 10);
				((CheckAckPayload)payload).setVersion(new String(versionArray));
				((CheckAckPayload)payload).setDeviceID(payloadArray[14]);
				break;
			case (byte) (XCaseCommon.REGISTER_TYPE|0x80):
				payload = new RegisterInfoPayload();
				payload.setType(payloadArray[0]);
				byte[] versionData = new byte[10];
				System.arraycopy(payloadArray, 1, versionData, 0, 10);
				((RegisterInfoPayload)payload).setVersion(new String(versionData));
				((RegisterInfoPayload)payload).setCentralID(payloadArray[11]);
				break;
			case XCaseCommon.NOTIFY_TYPE:
				payload = new NotifyPayload();
				payload.setType(payloadArray[0]);
				((NotifyPayload)payload).setId(payloadArray[1]);
				byte[] cmd = new byte[length-2];
				System.arraycopy(payloadArray, 2, cmd, 0, length-2);
				((NotifyPayload)payload).setCmd(cmd);
				break;
			}
			
		}
		
		return new XCaseMsg(header, crc, seq, length, payloadArray, payload);
	}

}
