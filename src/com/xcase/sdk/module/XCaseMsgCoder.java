package com.xcase.sdk.module;

import java.io.IOException;


public interface XCaseMsgCoder {

	public byte[] toXCase(XCaseMsg msg) throws IOException;
	public XCaseMsg fromXCase(byte[] input) throws IOException;
}
