package com.xcase.sdk.listener;

import java.util.EventObject;

import com.xcase.sdk.module.XCaseMsg;

public class CentralEvent extends EventObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private XCaseMsg msg;
	
	public CentralEvent(Object source) {
		super(source);
	}

	public XCaseMsg getMsg() {
		return msg;
	}

	public void setMsg(XCaseMsg msg) {
		this.msg = msg;
	}

	
	
	

}
