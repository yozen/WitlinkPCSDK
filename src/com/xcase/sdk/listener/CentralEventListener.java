package com.xcase.sdk.listener;

import java.util.EventListener;

public interface CentralEventListener extends EventListener{
	
	public void processEvent(CentralEvent event);
	
}
