package com.xcase.sdk.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CentralEventManager implements CentralEventListener{

	private final static Logger LOGGER = LoggerFactory.getLogger(CentralEventManager.class);
	
	@Override
	public void processEvent(CentralEvent event) {
		// TODO processEvent
		LOGGER.debug(event.getMsg().toString());
	}

}
