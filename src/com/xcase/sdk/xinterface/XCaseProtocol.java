package com.xcase.sdk.xinterface;

import java.awt.Color;
import java.util.List;

import com.xcase.sdk.arrangement.CubeGroup;
import com.xcase.sdk.arrangement.GroupMatrix;
import com.xcase.sdk.core.BleCube;

/**
 * Created by simon on 7/28/16.
 */
public interface XCaseProtocol {
	
	public static final int XCASE_ERROR_BLUETOOTH_OFF = 1; 		/** 蓝牙关闭 */
    public static final int XCASE_ERROR_NOT_SUPPORT_BLE = 2; 	/** 设备不支持BLE */

    public static final int CONNECT_PERIOD = 15 * 1000; 		/** 连接时间(毫秒) */
    public static final int AUTOCONNECT_INTERVAL = 15 * 1000; 	/** 自动连接间隔 */

    /**
     * 连接小块的回调
     */
    public interface ConnectCallback {
        /**
         * 返回连接上的小块
         * @param connectedCubes
         */
        public void onConnect(List<BleCube> connectedCubes);
        /**
         * 小块断开连接
         * @param cube
         */
        public void onDisconnect(BleCube cube);
    }

    /**
     * 小块触发事件的回调
     */
    public interface ActionCallback {
        /**
         * 两个小块相接触
         * @param group 接触后产生的小块组
         * @param cube1 其中一个小块
         * @param cube2 另外一个小块
         */
        public void onMerge(CubeGroup group, BleCube cube1, BleCube cube2);

        /**
         * 原本接触的两个小块被分开
         * @param cube1 其中一个小块
         * @param cube2 另外一个小块
         */
        public void onDivide(BleCube cube1, BleCube cube2);

        /**
         *小块摇晃事件
         * @param cube 摇晃的小块
         * @param times 摇晃的次数
         */
        public void onShake(BleCube cube, int times);

        /**
         * 小块旋转事件
         * @param cube 旋转的小块
         * @param fromOrientation 上一次的方位
         * @param toOrientation 旋转后的方位
         */
        public void onTurn(BleCube cube, int fromOrientation, int toOrientation);
    }

    /**
     * 向小块发送图片的回调
     */
    public interface SendImageCallback {
        /**
         * 发送图片成功
         */
        public void onSendImageSuccess();
        /**
         * 发送图片失败
         * @param cube 发送失败的小块
         * @param errorCode 错误代码
         */
        public void onSendImageFail(BleCube cube, int errorCode);
    }

    /**
     * 初始化，在首次获取XCaseManager后调用
     * @param activity app的Activity
     * @return 返回0表示初始化成功；1表示蓝牙关闭；2表示设备不支持BLE
     */
    public int initialize();

    /**
     * 请求连接小块：
     * 当count>0，在连接数达到count或到最大连接时长（变量CONNECT_PERIOD），返回连接的小块；
     * 当count=0，表示不限数量，到最大连接时长时返回连接的小块。
     * @param count 指定数量的小块，0为不限数量
     * @param connectCallback 返回连接的小块
     * @return 返回0表示初始化成功；1表示蓝牙关闭；2表示设备不支持BLE
     */
    public int requestCubes(int count, ConnectCallback connectCallback);

    /**
     * 每隔一段时间（变量AUTOCONNECT_INTERVAL），自动发现并连接小块，保证当前app连接上所有激活的小块
     * @param enable enable为true时打开，false为关闭。默认关闭
     */
    public void setAutoConnnectEnable(boolean enable);

    /**
     * 获取连接上的小块
     * @return 返回连接上的小块
     */
    public BleCube[] getCubes();

    /**
     * 获取当前所有的小块组
     * @return 返回所有的小块组
     */
    public CubeGroup[] getGroups();

    /**
     * 使小块显示图片，如果当前小块有缓存图片，且与指定的fileName一致，则直接显示；否则发送图片数据完成后显示
     * @param cubes 指定的小块
     * @param fileNames 需要发送的图片，注：图片资源需放在assets目录下！！！
     * @param callback 显示成功后的回调
     */
    public void displayCubesImages(BleCube[] cubes, String[] filePaths, SendImageCallback callback);

    /**
     * 向小块发送图片数据但不显示，完成后调用displayCubesImages会直接显示，提高效率
     * @param cubes 指定的小块
     * @param fileNames 需要发送的图片
     */
    public void cacheCubesImages(BleCube[] cubes, String[] filePaths);

    /**
     * 比较小块组是否匹配指定的序列
     * @param group 小块组
     * @param matrix 指定的序列
     * @return 匹配返回true，否则返回false
     */
    public boolean groupCompareMatrix(CubeGroup group, GroupMatrix matrix);

    /**
     *设置小块背景色
     * @param cube 指定的小块
     * @param rgbHex rgb值，如0xffffff
     */
    public void setCubeBackground(BleCube cube, int rgbHex);

    /**
     *给小块边框
     * @param cube 指定的小块
     * @param rgbHex rgb值，如0xffffff
     * @param framePix 边框像素值，最大是5
     */
    public void drawCubeFrame(BleCube cube, int rgbHex, int framePix);

    /**
     * 使小块闪烁
     * @param cube 指定的小块
     * @param twinkleNum 闪烁的次数
     */
    public void twinkleCube(BleCube cube, int twinkleNum);

    /**
     * 断开所有小块的连接
     */
    public void close();
    /**
     * 把字符串转成小块可识别的图片文件，之后可调用displayCubesImages方法发送图片，让小块显示字符串
     * @param text 要显示的字符串
     * @param fontColor 字体颜色
     * @param bgColor 背景颜色
     * @param fileDir 保存文件的目录
     * @return 返回图片文件的路径，作为displayCubesImages方法的参数
     */
    public String createStringImageFile(String text, int fontColor, int bgColor, String filePath);
}
