package com.xcase.sdk.xinterface;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xcase.sdk.arrangement.CubeGroup;
import com.xcase.sdk.arrangement.GroupMatrix;
import com.xcase.sdk.core.BleCube;
import com.xcase.sdk.core.Central;
import com.xcase.sdk.core.CentralManager;
import com.xcase.sdk.core.CentralProtocol.CentralCallback;
import com.xcase.sdk.util.DataUtil;
import com.xcase.sdk.util.RGBUtil;
import com.xcase.sdk.util.WLPUtil;

/**
 * Created by yozen on 9/22/16.
 */

public class XCaseManager implements XCaseProtocol {

	private final static Logger LOGGER = LoggerFactory.getLogger(XCaseManager.class);
	
    private boolean mIsScanning;
    private static final long SCAN_PERIOD = 15000;
    private int mNeedCubeNum;

    private static final int MAX_CUBES_PER_CENTRAL = 8;
    
    private final List<BleCube> connectedCubes;
    private final List<BleCube> discoveredCubes;

    private ActionCallback mActionCallback;
    private ConnectCallbackImpl connCallback;		//XCaseManager类的监听器
    private ConnectCallback mConnCallback;			//保留request方法传入的监听器
    private CentralCallback mCentralCallback;

    private ScheduledExecutorService scheduExec;
    private boolean scanResult = false;				//一次扫描标志
    
    private static XCaseManager instance = null;
    public boolean isScanning() {
        return mIsScanning;
    }
    
    private List<Central> centrals;
    private Queue<Central> scanQueue;	//扫描队列

    public static XCaseManager getInstance() {
        if (instance == null) {
            synchronized (XCaseManager.class) {
                if (instance == null) {
                    instance = new XCaseManager();
                }
            }
        }
        return instance;
    }

    private XCaseManager () {
        super();

        this.discoveredCubes = new CopyOnWriteArrayList<>();
        this.connectedCubes = new CopyOnWriteArrayList();
        this.connCallback = new ConnectCallbackImpl();
        this.scheduExec = Executors.newScheduledThreadPool(2);
        this.mCentralCallback = new CentralCallbackImpl();
        this.scanQueue = new LinkedBlockingQueue<Central>();
    }
    
    /** 初始化，检查是否有BLE主机 */ 
    public int initialize() {
    	try{
	    	CentralManager centralManager = CentralManager.getXCaseManager();
			centrals = centralManager.initSerial();
			if(centrals.size() <= 0){
				LOGGER.debug("not found valid usb central");
				return 0;
			}
    	}catch (Exception e) {
			// TODO: handle exception
    		e.printStackTrace();
    		return 0;
		}
        return 1;
    }

    public void setActionCallback(ActionCallback callback) {
        mActionCallback = callback;
    }
    
    private void scanTimeoutEvent(){
    	scheduExec.schedule(new Runnable() {
            public void run() {
                if(!scanResult){
                	LOGGER.info("scan timeout, got " + connectedCubes.size() + " cubes");
                	mConnCallback.onConnect(connectedCubes);
                }
                scanResult = false;
            }
        }, SCAN_PERIOD + 1000,TimeUnit.MILLISECONDS);
    }

    @Override
    public int requestCubes(int num, final ConnectCallback connectCallback) {
    	
    	this.mNeedCubeNum = num;
        this.discoveredCubes.clear();
        this.mConnCallback = connectCallback;
        
        if(num <= MAX_CUBES_PER_CENTRAL){
        	//如果请求的数量不超过8个，则直接用一个外设连接
        	centrals.get(0).setWantedCubeNum(num);
        	centrals.get(0).connectWithIDs(num, SCAN_PERIOD, this.connCallback, this.mCentralCallback);
        }else{
        	//测试两个外色的情况
        	centrals.get(0).setWantedCubeNum(MAX_CUBES_PER_CENTRAL);
        	centrals.get(0).connectWithIDs(MAX_CUBES_PER_CENTRAL, SCAN_PERIOD, this.connCallback, this.mCentralCallback);
//        	if(centrals.size() == 2){
//        		centrals.get(1).setWantedCubeNum(num-MAX_CUBES_PER_CENTRAL);
//        		this.scanQueue.add(centrals.get(1));
//        	}
        	centrals.get(1).setWantedCubeNum(MAX_CUBES_PER_CENTRAL);
        	this.scanQueue.add(centrals.get(1));
        	centrals.get(2).setWantedCubeNum(MAX_CUBES_PER_CENTRAL);
        	this.scanQueue.add(centrals.get(2));
        }
        //均分做法
//        int cubeNumPerCentral = num%centrals.size();
//        int cubeNumInLastCentral = 0;       
//        if(cubeNumPerCentral == 0){
//        	cubeNumPerCentral = num/centrals.size();
//        	cubeNumInLastCentral = cubeNumPerCentral;
//        }else{
//        	cubeNumPerCentral = num/centrals.size();
//        	cubeNumInLastCentral = cubeNumPerCentral + num%centrals.size();
//        }
//        for(int i=0; i<centrals.size(); i++){
//        	if(i == (centrals.size()-1)){
//        		//最后一个USB外设
//        		centrals.get(i).connectWithIDs(cubeNumInLastCentral, SCAN_PERIOD, this.connCallback);
//        	}else{
//        		centrals.get(i).connectWithIDs(cubeNumPerCentral, SCAN_PERIOD, this.connCallback);
//        	}
//        }
    	scanTimeoutEvent();
    	
        return 0;
    }
    
    private void sendStreamsToCubes(BleCube[] cubes, String[] fileNames, InputStream[] streams, 
    		final boolean isCache, final SendImageCallback callback) {
        final int[] count = {0};
        final int length = fileNames.length;
        for (int i = 0; i < length; i++) {
            final BleCube cube = cubes[i];
            final String imageName = fileNames[i];
            InputStream stream = streams[i];
            try{
	            cube.sendWLPImage(getBytesFromStream(stream), imageName, isCache, new SendImageCallback() {
	                @Override
	                public void onSendImageSuccess() {
	                    if (isCache) {
	                        cube.cacheImageName = imageName;
	                    } else {
	                        cube.contentName = imageName;
	                        cube.cacheImageName = null;
	                    }
	                    count[0]++;
	                    if (count[0] == length) {
	                        if (callback != null) {
	                            callback.onSendImageSuccess();
	                        }
	                    }
	                }
	
	                @Override
	                public void onSendImageFail(BleCube cube, int errorCode) {
	                    // TODO: 9/13/16 停止发送图片
	                    callback.onSendImageFail(cube,errorCode);
	                }
	            });
            }catch (Exception e) {
				// TODO: handle exception
            	e.printStackTrace();
			}
        }
    }

    private void sendImagesToCubes(BleCube[] cubes, String[] filePaths, boolean isCache, SendImageCallback callback) {
        List<InputStream> images = new CopyOnWriteArrayList<>();
        if (cubes.length < filePaths.length) {
            // TODO: 9/27/16 抛异常
            return;
        }
        for (String filePath : filePaths){
        	try{
        		File imageFile = new File(filePath);
        		DataInputStream imageStream = new DataInputStream(new FileInputStream(imageFile));
        		images.add(imageStream);
        	}catch (Exception e) {
				// TODO: handle exception
        		e.printStackTrace();
			}
        }
        InputStream[] streams = new InputStream[filePaths.length];
        images.toArray(streams);
        sendStreamsToCubes(cubes, filePaths, streams, isCache, callback);
    }
    
    /**
     * 从DataInputStream读出数据到byte数组
     * @param stream
     * @return
     */
    private byte[] getBytesFromStream(InputStream stream) {
    	List<Byte> imageData = new ArrayList<Byte>();
    	try{
	    	int ret = stream.read();
			imageData.clear();
			while(ret != -1){
				imageData.add((byte)ret);
				ret = stream.read();
			}
    	}catch (Exception e) {
			// TODO: handle exception
    		e.printStackTrace();
		}
		byte[] imageDataBytes = DataUtil.arrayListToBytes(imageData);
		
        return imageDataBytes;
    }

    @Override
    public void close() {
    	connectedCubes.clear();
    	discoveredCubes.clear();
    	for (Central central : centrals) {
			central.disconnectWithIDs(Central.ALL);
		}
    }    

    public boolean groupCompareMatrix(CubeGroup group, GroupMatrix matrix) {
        return group.compareMatrix(matrix);
    }

    public boolean groupsContainMatrix(int[][] matrix) {
        return false;
    }

    public boolean isSupportBle() {
        return true;
    }

    public boolean isBluetoothEnabled() {
        return true;
    }

    public int getNeedCubeNum() {
        return mNeedCubeNum;
    }
    
    public List<CubeGroup> allGroups() {
        List<CubeGroup> groups = new ArrayList<>();
        for (int i = 0; i < connectedCubes.size(); i++) {
            BleCube cube = connectedCubes.get(i);
            if (!groups.contains(cube.group)) {
                groups.add(cube.group);
            }
        }
        return groups;
    }

	@Override
	public void setCubeBackground(BleCube cube, int rgbColor) {
		// TODO setCubeBackground
		int rgb565 = RGBUtil.RGB888ToRGB565(rgbColor);
		byte[] cmd = cube.makeBackground(rgb565);
		cube.sendCmdWithID(cmd);
	}

	@Override
	public void drawCubeFrame(BleCube cube, int rgbColor, int framePix) {
		// TODO drawCubeFrame
		int rgb565 = RGBUtil.RGB888ToRGB565(rgbColor);
		byte[] cmd = cube.makeDrawFrame(rgb565, framePix);
		cube.sendCmdWithID(cmd);		
	}

	@Override
	public void twinkleCube(BleCube cube, int twinkleNum) {
		// TODO twinkleCube
		byte[] cmd = cube.makeTwinkle(twinkleNum);
		cube.sendCmdWithID(cmd);
	}

	
	private class ConnectCallbackImpl implements ConnectCallback{

		@Override
		public void onDisconnect(BleCube cube) {
			// TODO onDisconnect
			connectedCubes.remove(cube);
			mConnCallback.onDisconnect(cube);
		}
		
		@Override
		public void onConnect(List<BleCube> cubes) {
			// TODO onConnect
			for (BleCube bleCube : cubes) {
				if(!connectedCubes.contains(bleCube)){
					bleCube.actionCallback = mActionCallback;
					connectedCubes.add(bleCube);
				}
				LOGGER.info("onConnect, cubeID: " + bleCube.getCubeID());
			}
			
			if(connectedCubes.size() >= mNeedCubeNum){
				scanResult = true;
				mConnCallback.onConnect(connectedCubes);
			}
		}
		
	}
	
	private class CentralCallbackImpl implements CentralCallback{

		@Override
		public void onScanResult(int centralID, List<BleCube> cubes) {
			// TODO 返回外设扫描结果
			if(!scanQueue.isEmpty()){
				LOGGER.info("next central begin scan");
				Central central = scanQueue.poll();
				int wantedCubes = central.getWantedCubeNum();
				central.connectWithIDs(wantedCubes, SCAN_PERIOD, connCallback, mCentralCallback);
			}
			
		}
		
	}

	@Override
	public void setAutoConnnectEnable(boolean enable) {
		// TODO setAutoConnnectEnable
		
	}

	@Override
	public BleCube[] getCubes() {
		// TODO getCubes
		BleCube[] cubes = new BleCube[connectedCubes.size()];
        return connectedCubes.toArray(cubes);
	}

	@Override
	public CubeGroup[] getGroups() {
		// TODO getGroups
		List<CubeGroup> list = new ArrayList<>();
        for (int i = 0; i < connectedCubes.size(); i++) {
            BleCube cube = connectedCubes.get(i);
            if (!list.contains(cube.group)) {
                list.add(cube.group);
            }
        }
        CubeGroup[] groups = new CubeGroup[list.size()];
        return list.toArray(groups);
	}

	@Override
	public void displayCubesImages(BleCube[] cubes, String[] fileNames, SendImageCallback callback) {
		// TODO displayCubesImages
		sendImagesToCubes(cubes, fileNames, false, callback);
	}

	@Override
	public void cacheCubesImages(BleCube[] cubes, String[] fileNames) {
		// TODO cacheCubesImages
		sendImagesToCubes(cubes, fileNames, true, null);
	}

	@Override
	public String createStringImageFile(String text, int fontColor, int bgColor, String filePath) {
		// TODO createStringImageFile
		String path = WLPUtil.convertStringToWLPBytes(text, fontColor, bgColor, filePath);
		return path;
	}
	
}

