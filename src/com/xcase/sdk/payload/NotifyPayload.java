package com.xcase.sdk.payload;

public class NotifyPayload extends BasicPayload{

	private int id;
	private byte[] cmd;
	
	public int getId() {
		return id;
	}
	public void setId(byte id) {
		this.id = id;
	}
	public byte[] getCmd() {
		return cmd;
	}
	public void setCmd(byte[] cmd) {
		this.cmd = cmd;
	}
}
