package com.xcase.sdk.payload;

public class BasicPayload {
	
	private byte type;

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}
	
}
