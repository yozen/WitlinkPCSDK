package com.xcase.sdk.payload;

public class ScanAckPayload extends BasicPayload{

	private byte connNum;
	private int connIDs;
	private int deviceID;
	
	public byte getConnNum() {
		return connNum;
	}
	public void setConnNum(byte connNum) {
		this.connNum = connNum;
	}
	public int getConnIDs() {
		return connIDs;
	}
	public void setConnIDs(int connIDs) {
		this.connIDs = connIDs;
	}
	public int getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(int deviceID) {
		this.deviceID = deviceID;
	}
	
}
