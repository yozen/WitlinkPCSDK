package com.xcase.sdk.payload;

public class DisconnPayload extends BasicPayload{
	
	private int disNum;
	private int disIDs;
	
	public int getDisNum() {
		return disNum;
	}
	public void setDisNum(int disNum) {
		this.disNum = disNum;
	}
	public int getDisIDs() {
		return disIDs;
	}
	public void setDisIDs(int disIDs) {
		this.disIDs = disIDs;
	}
}
