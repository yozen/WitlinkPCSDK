package com.xcase.sdk.payload;


public class CommadPayload extends BasicPayload{

	private byte id;
	private byte cmdLen;
	private byte[] cmd;
	
	public byte getId() {
		return id;
	}
	public void setId(byte id) {
		this.id = id;
	}
	public byte getCmdLen() {
		return cmdLen;
	}
	public void setCmdLen(byte cmdLen) {
		this.cmdLen = cmdLen;
	}
	public byte[] getCmd() {
		return cmd;
	}
	public void setCmd(byte[] cmd) {
		this.cmd = cmd;
	}
}
