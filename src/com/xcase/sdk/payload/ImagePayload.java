package com.xcase.sdk.payload;

public class ImagePayload extends BasicPayload{

	private byte id;
	private byte isColorTable;
	private byte imageLen;
	private byte[] images;
	
	public byte getId() {
		return id;
	}
	public void setId(byte id) {
		this.id = id;
	}
	public byte getIsColorTable() {
		return isColorTable;
	}
	public void setIsColorTable(byte isColorTable) {
		this.isColorTable = isColorTable;
	}
	public byte getImageLen() {
		return imageLen;
	}
	public void setImageLen(byte imageLen) {
		this.imageLen = imageLen;
	}
	public byte[] getImages() {
		return images;
	}
	public void setImages(byte[] images) {
		this.images = images;
	}

}
