package com.xcase.sdk.payload;

public class RegisterInfoPayload extends BasicPayload{
	
	private String version;
	private int centralID;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public int getCentralID() {
		return centralID;
	}
	public void setCentralID(int centralID) {
		this.centralID = centralID;
	}
}
