package com.xcase.sdk.payload;


public class ScanPayload extends BasicPayload{

	private byte scanOn;
	private byte maxConn;
	private long timeout;
	
	public byte getScanOn() {
		return scanOn;
	}
	public void setScanOn(byte scanOn) {
		this.scanOn = scanOn;
	}
	public byte getMaxConn() {
		return maxConn;
	}
	public void setMaxConn(byte maxConn) {
		this.maxConn = maxConn;
	}
	public long getTimeout() {
		return timeout;
	}
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
}
